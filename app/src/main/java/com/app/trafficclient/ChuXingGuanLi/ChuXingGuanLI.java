package com.app.trafficclient.ChuXingGuanLi;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trafficclient.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ChuXingGuanLI extends AppCompatActivity {
    Context context;
    int day;
    static DialogRiQi myDialog;
    private TextView txvTime;
    private TextView txvXianHao;
    private Switch swit1;
    private Switch swit2;
    private Switch swit3;
    Handler handler;
    private LinearLayout linearlayout;
    private ImageView mainImageview;
    SimpleDateFormat format = new SimpleDateFormat("yyyy年M月dd日");
    String xiTongTime,xiTongDay;
    SimpleDateFormat formatDay = new SimpleDateFormat("dd");
    String danhao ="单号出行车辆：1号、3号";
    String shuanghao = "双号出行车辆：2号";
    boolean flag =true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chu_xing_guan_li);
        initView();
        initThread();
        initData();
        initListener();
    }

    private void initThread() {

        handler =new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what){
                    case 1: mainImageview.setBackgroundDrawable(getResources().getDrawable(R.drawable.red));break;
                    case 2: mainImageview.setBackgroundDrawable(getResources().getDrawable(R.drawable.yellow));break;
                    case 3: mainImageview.setBackgroundDrawable(getResources().getDrawable(R.drawable.green));break;
                }
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (flag){
                    handler.sendEmptyMessage(1);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(2);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(3);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } //while
            }
        }).start();

    }

    @Override
    protected void onDestroy() {
        flag =false;
        super.onDestroy();
    }

    private void initListener() {

        txvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog =new DialogRiQi(context);
                myDialog.setCanceledOnTouchOutside(false);
                myDialog.show();

                myDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (DialogRiQi.getriqi() == null){
                            return;
                        }
                        txvTime.setText(DialogRiQi.getriqi());
                        if (DialogRiQi.getXinXi().equals("1")){
                            danHaoSet();
                        }else {
                            shuangHaoSet();
                        }
                    }
                }); //DismissListener
            }
        });
    }

    private void initData() {
        context = this;
        Date date = new Date(System.currentTimeMillis());
        xiTongTime = format.format(date);
        xiTongDay = formatDay.format(date);
        txvTime.setText(xiTongTime);
        day = Integer.parseInt(xiTongDay);

        if (day%2==0){
            shuangHaoSet();
        }else{
            danHaoSet();
        }

    } //initdata

    public void danHaoSet(){
        txvXianHao.setText(danhao);
        swit1.setEnabled(true);
        swit1.setChecked(true);

        swit2.setEnabled(false);
        swit2.setChecked(false);

        swit3.setEnabled(true);
        swit3.setChecked(true);
    }
    public void shuangHaoSet(){
        txvXianHao.setText(shuanghao);
        swit1.setChecked(false);
        swit1.setEnabled(false);

        swit2.setEnabled(true);
        swit2.setChecked(true);

        swit3.setChecked(false);
        swit3.setEnabled(false);
    }
    public  static  void dissMiss(){
        myDialog.dismiss();
    }

    private void initView() {
        txvTime = findViewById(R.id.chuXing_txv_time);
        txvXianHao = findViewById(R.id.chuXing_txv_xianHao);
        swit1 = findViewById(R.id.chuXing_swit_1);
        swit2 = findViewById(R.id.chuXing_swit_2);
        swit3 = findViewById(R.id.chuXing_swit_3);
        mainImageview = findViewById(R.id.chuXing_img_rg);
    }

}
