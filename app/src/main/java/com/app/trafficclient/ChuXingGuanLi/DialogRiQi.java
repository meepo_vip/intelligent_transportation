package com.app.trafficclient.ChuXingGuanLi;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;

import com.app.trafficclient.R;

public class DialogRiQi extends Dialog {
    static Dialog dialog;
    static String riqi;
    static  int day;
    CalendarView calendarView;
    private Button save;
    static String chuXingXinXi;

    public DialogRiQi( Context context) {
        super(context);
        setContentView(R.layout.dialog_chu_xing_ri_li);

        calendarView = findViewById(R.id.rili);
        save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChuXingGuanLI.dissMiss();
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                riqi = year+"年"+(month+1)+"月"+dayOfMonth+"日";
                day = dayOfMonth;
                if (day%2 == 0){
                    chuXingXinXi="2";
                }else {
                    chuXingXinXi ="1";
                }
            }
        });
    }

    public static String getriqi(){
        return riqi;
    }
    public static String getXinXi(){
        return chuXingXinXi;
    }
}
