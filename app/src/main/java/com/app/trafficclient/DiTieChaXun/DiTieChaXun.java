package com.app.trafficclient.DiTieChaXun;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.app.trafficclient.R;
import com.app.trafficclient.util.GetUrl;
import com.app.trafficclient.util.MyLoadDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiTieChaXun extends AppCompatActivity {
    private ListView ditieListView;
    Context context;
    RequestQueue requestQueue;
    List<BeanDiTieXinXi.ROWS_DETAILEntity> list ;
//    static Bitmap [] bitmaps;
    static List<Bitmap> bitmaps;

    public static List<Bitmap> getBitmaps() {
        return bitmaps;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_di_tie_cha_xun);
        ditieListView = findViewById(R.id.ditie_listView);
        context= this;
        bitmaps = new ArrayList<>();
        list = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(context);
        sendPost();
        ditieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               newIntent(position);
            }
        });

    }

    private void sendPost() {
        MyLoadDialog myLoadDialog = new MyLoadDialog(context);
        MyLoadDialog.showDia();
        String url = GetUrl.getUrl() +"action/GetMetroInfo.do";
        Map<String,Object> map =new HashMap<>();
        map.put("Line",0);
        map.put("UserName","user1");
        JSONObject jsonObject = new JSONObject(map);
        JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url,jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                String string = jsonObject.toString();
                Gson gson = new Gson();
                BeanDiTieXinXi beanDiTieXinXi = gson.fromJson(string,BeanDiTieXinXi.class);
                list = beanDiTieXinXi.getROWS_DETAIL();
                for (int i =0;i<list.size();i++) {
                    String imgUrl = GetUrl.getUrl()+ beanDiTieXinXi.getROWS_DETAIL().get(i).getMap();
//                   Log.d("tagurl",imgUrl);
                    final int finalI = i;
                    ImageRequest imageRequest = new ImageRequest(imgUrl, new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            bitmaps.add(bitmap);
//                           Log.d("tagbitmap",bitmaps.size()+"");
                        }
                    }, 0, 0, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                        }
                    });
                    requestQueue.add(imageRequest);
                }
                MyLoadDialog.disDia();
                ActivityDiTieItemAdapter adapter = new ActivityDiTieItemAdapter(context,list);
                ditieListView.setAdapter(adapter);
                Log.d("taggg",list.size()+"");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("taggggg",volleyError.toString());
            }
        });
        requestQueue.add(jsonRequest);
    }

    private void newIntent(int position) {
        Intent intent = new Intent();
        intent.setClass(context,DiTuMoXing.class);
        intent.putExtra("flag",position);
        startActivity(intent);
    }
}
