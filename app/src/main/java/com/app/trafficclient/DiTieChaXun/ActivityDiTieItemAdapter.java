package com.app.trafficclient.DiTieChaXun;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.trafficclient.R;

public class ActivityDiTieItemAdapter extends BaseAdapter {

    private final List<BeanDiTieXinXi.ROWS_DETAILEntity> list;

    int i=1;
    private Context context;
    private LayoutInflater layoutInflater;

    public ActivityDiTieItemAdapter(Context context,List<BeanDiTieXinXi.ROWS_DETAILEntity> list) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        Log.d("object.size===>",list.size()+"");
        return list.size();

    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.activity_di_tie_item, null);
            ViewHolder viewHolder = new ViewHolder(convertView);
            viewHolder.ditieItem.setText("线路图"+i+"号");
            i++;
        }
        return convertView;
    }


    protected class ViewHolder {
        private TextView ditieItem;

        public ViewHolder(View view) {
            ditieItem = (TextView) view.findViewById(R.id.ditie_item);
        }
    }
}

