package com.app.trafficclient.DiTieChaXun;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.app.trafficclient.R;

import java.util.ArrayList;
import java.util.List;

public class DiTuMoXing extends AppCompatActivity {
    private ImageView diTuImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_di_tu_mo_xing);

        diTuImg = findViewById(R.id.diTu_img);
        Intent intent =getIntent();
        int flag = intent.getIntExtra("flag",0);
        diTuImg.setImageBitmap(DiTieChaXun.getBitmaps().get(flag));

    }
}
