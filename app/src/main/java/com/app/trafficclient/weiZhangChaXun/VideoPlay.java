package com.app.trafficclient.weiZhangChaXun;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.app.trafficclient.R;

public class VideoPlay extends AppCompatActivity {
    VideoView videoView;
    int video_id[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        videoView = findViewById(R.id.videoPlay_video);
        Intent intent=getIntent();
        int i = intent.getIntExtra("flag",0);
        //设置视屏的来源
        String path = "android.resource://" + getApplicationContext().getPackageName() + "/";
        video_id = new int[]{R.raw.v1,R.raw.v1,R.raw.v1,R.raw.v1,R.raw.v1}; //视频id


            //设置视频控制器
            videoView.setMediaController(new MediaController(this));
            //设置视频路径
            videoView.setVideoPath(path + video_id[i]);
            videoView.start();
           // 隐藏进度条
           // view[i] .setMediaController(mc);
    }
}
