package com.app.trafficclient;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.trafficclient.weiZhangChaXun.Image;
import com.app.trafficclient.weiZhangChaXun.VideoPlay;
import com.app.trafficclient.weiZhangChaXun.videos;

public class weiZhangChaKan extends AppCompatActivity implements View.OnTouchListener{

    ImageView img1,img2,img3,img4,img5;
    ImageView imgview[];

    int video_id[];   //5个视频的id
    TextView t1,t2,t3,t4,t5; //视频下方文本
    TextView txvview[];

    ImageView bac;
    TextView txv;

    RadioGroup rgp;
    RadioButton rbVideo,rbImage;

    String path; //R....之前
    Uri uri[] ;  //完整的  通过path  + R.***

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.illegal);
        initView();
        initData();
        initListener();

    }

    private void initData() {

        path="android.resource://" + getApplicationContext().getPackageName() + "/";
        imgview = new ImageView[]{img1,img2,img3,img4,img5}; //图片控件
        txvview = new TextView[]{t1,t2,t3,t4,t5};
        video_id = new int[]{R.raw.v1,R.raw.v1,R.raw.v1,R.raw.v1,R.raw.v1}; //视频id
        uri= new Uri[5];
        for (int i =0;i<5;i++){
            uri[i] = Uri.parse(path + video_id[i]);  //转换uri
            txvview[i].setText("违章"+String.valueOf(i+1));
            imgview[i].setImageBitmap(SuoLueTu(uri[i]));
        }


    }

    public void initListener(){

        img1.setOnTouchListener(this);
        img2.setOnTouchListener(this);
        img3.setOnTouchListener(this);
        img4.setOnTouchListener(this);
        img5.setOnTouchListener(this);

bac.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});
      rgp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){

                    case R.id.ill_rbVideo:
                        rbImage.setBackgroundColor(Color.parseColor("#FFFFFF"));//选中改变颜色
                        rbVideo.setBackgroundColor(Color.parseColor("#c0c0c0c0"));
                        videos video = new videos();
                        getFragmentManager().beginTransaction().replace(R.id.ill_relative_layout,video).commit();
                       break;

                    case R.id.ill_rbImage:
                        rbVideo.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        rbImage.setBackgroundColor(Color.parseColor("#c0c0c0c0"));
                        Image image = new Image();
                        getFragmentManager().beginTransaction().replace(R.id.ill_relative_layout,image).commit();
                        break;

                }//switch

            }

        });

    }
    private void initView() {

        img1 =  findViewById(R.id.ill_fragment_Image_video1);
        img2 =  findViewById(R.id.ill_fragment_Image_video2);
        img3 =  findViewById(R.id.ill_fragment_Image_video3);
        img4 =  findViewById(R.id.ill_fragment_Image_video4);
        img5 =  findViewById(R.id.ill_fragment_Image_video5);

        t1 = findViewById(R.id.ill_t1);
        t2 = findViewById(R.id.ill_t2);
        t3 = findViewById(R.id.ill_t3);
        t4 = findViewById(R.id.ill_t4);
        t5 = findViewById(R.id.ill_t5);
        rgp =findViewById(R.id.ill_rgp);//违章视频和图片选择

        bac = findViewById(R.id.imageView_back);
        bac.setImageResource(R.drawable.change);

        txv = findViewById(R.id.tv_title);
        txv.setText("车辆违章");

        rbVideo = findViewById(R.id.ill_rbVideo);
        rbImage = findViewById(R.id.ill_rbImage);
        rbVideo.setChecked(true);  //默认选中


    }

    private Bitmap SuoLueTu(Uri aVideoUri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, aVideoUri);
        Bitmap bitmap = retriever.getFrameAtTime(1*1000*1000, MediaMetadataRetriever.OPTION_PREVIOUS_SYNC);
        return bitmap;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.ill_fragment_Image_video1:
               start(0);
                break;
            case R.id.ill_fragment_Image_video2:
               start(1);
                break;
            case R.id.ill_fragment_Image_video3:
                start(2);
                break;
            case R.id.ill_fragment_Image_video4:
                start(3);
                break;
            case R.id.ill_fragment_Image_video5:
                start(4);
                break;
        }

        return false;
    }

    private void start(int flag) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(),VideoPlay.class);
        intent.putExtra("flag",flag);
        startActivity(intent);
    }


}//class
