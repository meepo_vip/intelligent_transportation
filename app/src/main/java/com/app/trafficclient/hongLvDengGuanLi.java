package com.app.trafficclient;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class hongLvDengGuanLi extends AppCompatActivity {
    TextView title;
    ImageView back;
    Spinner spinner;
    Button sel;
    String[] data2 = {"1","9","9","9"};
    String[] data3 = {"2","8","8","8"};
    String[] data4 = {"3","7","8","7"};
    int flag;
    List<TextView> txv_line2;
    List<TextView> txv_line3;
    List<TextView> txv_line4;
    Resources res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rg_light);
        initView();
        initListen();

    }

    private void initListen() {

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                flag=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
              return;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (flag){
                    case 0:
                    case 3:
                    case 5:
                    case 7:setData(data2,data3,data4);break;
                    case 1:
                    case 2:
                    case 4:
                    case 6: setData(data4,data3,data2);break;

                }
            }
        });


    }

    private void initView() {
        spinner = findViewById(R.id.RGLight_spinner);
        sel = findViewById(R.id.RGLight_sel);
        title = findViewById(R.id.tv_title);
        title.setText("红绿灯管理");
        back = findViewById(R.id.imageView_back);
        back.setImageResource(R.drawable.change);

        txv_line2 = new ArrayList<TextView>();
        txv_line3 = new ArrayList<TextView>();
        txv_line4 = new ArrayList<TextView>();

        res = getResources();
        for (int i = 1; i <= 4; i++) {
            int id2 = res.getIdentifier("table2_" + i, "id", getPackageName());
            int id3 = res.getIdentifier("table3_" + i, "id", getPackageName());
            int id4 = res.getIdentifier("table4_" + i, "id", getPackageName());
            TextView txv2 = (TextView) findViewById(id2);
            TextView txv3 = (TextView) findViewById(id3);
            TextView txv4 = (TextView) findViewById(id4);
            txv_line2.add(txv2);
            txv_line3.add(txv3);
            txv_line4.add(txv4);

            txv_line2.get(i-1).setText(data2[i-1]);
            txv_line3.get(i-1).setText(data3[i-1]);
            txv_line4.get(i-1).setText(data4[i-1]);
        }

    }//initview

    public  void setData(String data2[],String data3[],String data4[]){
        for (int i=0;i<=3;i++){
            txv_line2.get(i).setText(data2[i]);
            txv_line3.get(i).setText(data3[i]);
            txv_line4.get(i).setText(data4[i]);
        }
    }

}
