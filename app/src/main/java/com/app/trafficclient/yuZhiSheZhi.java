package com.app.trafficclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class yuZhiSheZhi extends AppCompatActivity {
    private ImageView imageViewBack;
    private TextView tvTitle;
    private TextView settingTxv;
    private EditText settingWarnEditWendu;
    private EditText settingWarnEditShidu;
    private EditText settingWarnEditGuangzhao;
    private EditText settingWarnEditCO2;
    private EditText settingWarnEditPM2;
    private EditText settingWarnEditDaolu;
    private Button settingWarnBtnSave;
    private Switch settingWarnSwitch;

   private int wenDu,shiDu,guangZhao,co2,pm2,daoLu;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_warn);
        initView();
        initListen();

        if (sharedPreferences.getInt("flag",0) == 1){
            settingWarnSwitch.setChecked(true);
            show();
        }else {
            settingWarnSwitch.setChecked(false);
        }
    }//onCreate

    //失去焦点
    private void lostFouce(){
        settingWarnEditWendu    .setFocusable(false);
        settingWarnEditShidu    .setFocusable(false);
        settingWarnEditGuangzhao.setFocusable(false);
        settingWarnEditCO2      .setFocusable(false);
        settingWarnEditPM2      .setFocusable(false);
        settingWarnEditDaolu    .setFocusable(false);
    }

    //显示设置过的值
    private void show() {
        String defaultValue = "";
        settingWarnEditWendu.    setText(sharedPreferences.getString("wendu",     defaultValue) );
        settingWarnEditShidu.    setText(sharedPreferences.getString("shidu",     defaultValue) );
        settingWarnEditGuangzhao.setText(sharedPreferences.getString("guangzhao", defaultValue) );
        settingWarnEditCO2.      setText(sharedPreferences.getString("co2",       defaultValue) );
        settingWarnEditPM2.      setText(sharedPreferences.getString("pm2",       defaultValue) );
        settingWarnEditDaolu.    setText(sharedPreferences.getString("daolu",     defaultValue) );
    }

    private boolean txvIsNull() {
        if (settingWarnEditWendu.getText().toString().trim().length()     != 0 &&
            settingWarnEditShidu.getText().toString().trim().length()     != 0 &&
            settingWarnEditGuangzhao.getText().toString().trim().length() != 0 &&
            settingWarnEditCO2.getText().toString().trim().length()       != 0 &&
            settingWarnEditPM2.getText().toString().trim().length()       != 0 &&
            settingWarnEditDaolu.getText().toString().trim().length()     != 0){
            return true;
        }else {
            return false;
        }

    }
    private void initListen() {
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        settingWarnBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( txvIsNull() ) {
                    //把文本框的值取出，转换int
                    wenDu     = Integer.parseInt(settingWarnEditWendu    .getText().toString().trim());
                    shiDu     = Integer.parseInt(settingWarnEditShidu    .getText().toString().trim());
                    guangZhao = Integer.parseInt(settingWarnEditGuangzhao.getText().toString().trim());
                    co2       = Integer.parseInt(settingWarnEditCO2      .getText().toString().trim());
                    pm2       = Integer.parseInt(settingWarnEditPM2      .getText().toString().trim());
                    daoLu     = Integer.parseInt(settingWarnEditDaolu    .getText().toString().trim());

                    //把取出的值写到SharedPerence
                    editor.putString("wendu",     String.valueOf(wenDu)    );
                    editor.putString("shidu",     String.valueOf(shiDu)    );
                    editor.putString("guangzhao", String.valueOf(guangZhao));
                    editor.putString("co2",       String.valueOf(co2)      );
                    editor.putString("pm2",       String.valueOf(pm2)      );
                    editor.putString("daolu",     String.valueOf(daoLu)    );

                    editor.putInt("flag", 1); //设置开关，标记
                    editor.commit();
                    lostFouce();
                    Toast.makeText(getApplicationContext(),"保存完成",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"请先输入数据",Toast.LENGTH_SHORT).show();
                }
            } //onclick
        });

        settingWarnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (settingWarnSwitch.isChecked()){
                    settingTxv.setText("开");
                        //设置可用
                        settingWarnEditWendu    .setEnabled(true);
                        settingWarnEditShidu    .setEnabled(true);
                        settingWarnEditGuangzhao.setEnabled(true);
                        settingWarnEditCO2      .setEnabled(true);
                        settingWarnEditPM2      .setEnabled(true);
                        settingWarnEditDaolu    .setEnabled(true);
                        show();

                    // / 在这里进行 APP每隔10秒对设置值检测，当低于阈值时，APP向Android设备状态栏发送一次告警通知，
                    // 告警通知内容包括检测对象、阈值、当前值。例如：湿度报警，阈值80，当前值85。
                }else {
                    editor.putInt("flag", 0).commit();
                    settingTxv.setText("关");
                    settingWarnEditWendu.setEnabled(false);
                    settingWarnEditShidu.setEnabled(false);
                    settingWarnEditGuangzhao.setEnabled(false);
                    settingWarnEditCO2.setEnabled(false);
                    settingWarnEditPM2.setEnabled(false);
                    settingWarnEditDaolu.setEnabled(false);
                }
            }//public void onCheckedChanged
        });



    }//initlisten

    private void initView() {
        sharedPreferences = getSharedPreferences("warmSetting", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        settingWarnSwitch = findViewById(R.id.setting_warn_Switch);
        imageViewBack = findViewById(R.id.imageView_back);
        imageViewBack.setImageResource(R.drawable.change);
        tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText("阈值设置");
        settingTxv = findViewById(R.id.setting_txv);//开关状态

        settingWarnEditWendu = findViewById(R.id.setting_warn_editWendu);
        settingWarnEditShidu = findViewById(R.id.setting_warn_editShidu);
        settingWarnEditGuangzhao = findViewById(R.id.setting_warn_editGuangzhao);
        settingWarnEditCO2 = findViewById(R.id.setting_warn_editCO2);
        settingWarnEditPM2 = findViewById(R.id.setting_warn_editPM2);
        settingWarnEditDaolu = findViewById(R.id.setting_warn_editDaolu);

        settingWarnBtnSave = findViewById(R.id.setting_warn_btnSave);

    }
}
