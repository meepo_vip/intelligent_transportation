package com.app.trafficclient.erWeiMa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trafficclient.R;

public class ErWeiMa extends AppCompatActivity {
    private ImageView imageView_back;
    private TextView tv_title;
    private Spinner cheliangbianhao;
    private EditText chongzhijine;
    private EditText shuaxinzhouqi;
    private Button shengchengerweima;
    String num, money, time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_er__wei__ma);
        initView();
    }

    private void initView() {
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        cheliangbianhao = (Spinner) findViewById(R.id.cheliangbianhao);
        chongzhijine = (EditText) findViewById(R.id.chongzhijine);
        shuaxinzhouqi = (EditText) findViewById(R.id.shuaxinzhouqi);
        shengchengerweima = (Button) findViewById(R.id.shengchengerweima);
        tv_title.setText("二维码支付");
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        shengchengerweima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = cheliangbianhao.getSelectedItem().toString();
                money = chongzhijine.getText().toString();
                time = shuaxinzhouqi.getText().toString();
                if (money.length() > 0 && time.length() > 0) {
                    int aa = Integer.parseInt(time);
                    int bb = Integer.parseInt(money);
                    if (aa > 0 && bb > 0) {
                        Intent intent = new Intent(getApplicationContext(), ShengCheng.class);
                        intent.putExtra("车辆号码", num);
                        intent.putExtra("充值金额", money);
                        intent.putExtra("刷新周期", time);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "请输入正确数据", Toast.LENGTH_SHORT).show();
                    }

                }else
                {  Toast.makeText(getApplicationContext(), "请检查数据是否输入完整", Toast.LENGTH_SHORT).show();}

            }
        });
    }

}
