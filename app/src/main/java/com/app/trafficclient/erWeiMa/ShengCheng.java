package com.app.trafficclient.erWeiMa;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.trafficclient.R;
import com.app.trafficclient.util.LoadingDialog;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.Hashtable;

public class ShengCheng extends AppCompatActivity {
    private ImageView imageView_back;
    private TextView tv_title,showerweima;
    private ImageView erweima;

    String money1;
    int time;
    String spinner_value;
    String money;
    Handler handler;
    Bitmap bitmap;
    boolean flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shengchengerweima);
        initView();
        initdata();
        timer();
    }
    private void initView() {
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        erweima = (ImageView) findViewById(R.id.erweima);
        showerweima=findViewById(R.id.erweimaxinxi);
        tv_title.setText("二维码");
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        erweima.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showerweima.setText("车辆编号="+spinner_value+","+"充值金额="+money+"元");
                return true;
            }
        });
    }

    public void initdata(){
        flag =true;
        spinner_value=getIntent().getStringExtra("车辆号码");
        money=getIntent().getStringExtra("充值金额");
        time= Integer.parseInt(getIntent().getStringExtra("刷新周期"))*1000;
        //tim= Integer.parseInt(time);
//        tim=tim*1000;
    }

    public void chuangjianerweima(String url) {
        int  w=222;
        int h=222;
//        Log.d("12333",w +"  "+h);

        try {
            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            //图像数据转换，使用了矩阵转换
            BitMatrix bitMatrix = new QRCodeWriter().encode(url, BarcodeFormat.QR_CODE, w, h, hints);
            int[] pixels = new int[w * h];
            //下面这里按照二维码的算法，逐个生成二维码的图片，
            //两个for循环是图片横列扫描的结果
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * w + x] = Color.parseColor("#000000");
                    }else {
                        pixels[y * w + x] = Color.parseColor("#FFFFFF");
                    }
                }
            }
            //生成二维码图片的格式，使用ARGB_8888
            /*Bitmap*/
            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        }
        catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private void timer() {

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    chuangjianerweima(spinner_value + "号车付款" + money + "元 ");
                    erweima.setImageBitmap(bitmap);
                }else {
                    chuangjianerweima(spinner_value + "号车付款" + money + "元  ");
                    erweima.setImageBitmap(bitmap);
                }
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (flag){
                    handler.sendEmptyMessage(1);
                    try {
                        Thread.sleep(time);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(2);
                    try {
                        Thread.sleep(time);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();



    } //方法结束

    @Override
    protected void onDestroy() {
        flag =false;
        super.onDestroy();
    }
}
