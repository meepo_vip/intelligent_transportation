package com.app.trafficclient.geRenZhongXin;

import java.util.List;

public class BeanFrag1Cars {

    /**
     * cars : [
     * {
     * "owner":"370101196101011001","license":"赣B12345","balance":1178,
     * "purchase":1547913600000,"id":1,"brand":"audi","status":0
     * },
     * {
     * "owner":"370101196101011001","license":"京A6666","balance":10899,
     * "purchase":1548777600000,"id":2,"brand":"bugatti veyron","status":0
     * }
     *      ]
     *
     * statusInfo : 请求成功
     * account : user1
     * status : 1
     */
    private List<CarsEntity> cars;
    private String statusInfo;
    private String account;
    private int status;

    public void setCars(List<CarsEntity> cars) {
        this.cars = cars;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<CarsEntity> getCars() {
        return cars;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public String getAccount() {
        return account;
    }

    public int getStatus() {
        return status;
    }

    public class CarsEntity {
        /**
         * owner : 370101196101011001
         * license : 赣B12345
         * balance : 1178
         * purchase : 1547913600000
         * id : 1
         * brand : audi
         * status : 0
         */
        private String owner;
        private String license;
        private int balance;
        private long purchase;
        private int id;
        private String brand;
        private int status;

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }

        public void setPurchase(long purchase) {
            this.purchase = purchase;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getOwner() {
            return owner;
        }

        public String getLicense() {
            return license;
        }

        public int getBalance() {
            return balance;
        }

        public long getPurchase() {
            return purchase;
        }

        public int getId() {
            return id;
        }

        public String getBrand() {
            return brand;
        }

        public int getStatus() {
            return status;
        }
    }
}
