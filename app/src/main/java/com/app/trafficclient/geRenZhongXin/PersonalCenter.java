package com.app.trafficclient.geRenZhongXin;

import android.graphics.Matrix;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.trafficclient.R;

import java.util.ArrayList;
import java.util.List;

public class PersonalCenter extends AppCompatActivity implements View.OnClickListener,
Fragment1.OnFragmentInteractionListener ,
Fragment2.OnFragmentInteractionListener,
Fragment3.OnFragmentInteractionListener{

    private ImageView imageViewBack;
    private TextView tvTitle;

    private TextView personalItemTv1;
    private TextView personalItemTv2;
    private TextView personalItemTv3;

    private ImageView dongHuaTu;
    private ViewPager personalViewPager;

    private List<Fragment> list;
    private MyFragmentPagerAdapter adapter;
    private int pianYi = 0;//移动条图片的偏移量
    private int currIndex = 0;//当前页面的编号
    private int one ; //移动条滑动一页的距离
    private int two ; //两页的距离

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_center);
        initView();
        initListener();
        initData();
    }

    private void initData() {
        list = new ArrayList<>();
        list.add(new Fragment1());
        list.add(new Fragment2());
        list.add(new Fragment3());

        adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),list);
        personalViewPager.setAdapter(adapter);
        personalViewPager.setOffscreenPageLimit(2); //预加载
        personalViewPager.setCurrentItem(0);//初始化显示第一个页面

        dongHuaTu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                pianYi = dongHuaTu.getMeasuredWidth()+15;
                one = pianYi;
                two = 2 * one;
            }
        });

   }

    private void initListener() {
         imageViewBack.setOnClickListener(this);
         personalItemTv1.setOnClickListener(this);
         personalItemTv2.setOnClickListener(this);
         personalItemTv3.setOnClickListener(this);
         personalViewPager.setOnPageChangeListener(new MyPageChangeListener());

    }

    private void initView(){
        imageViewBack = findViewById(R.id.imageView_back);
        tvTitle = findViewById(R.id.tv_title);
        tvTitle.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        tvTitle.setText("个人中心");
        imageViewBack.setImageResource(R.drawable.change);
        imageViewBack.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

        personalItemTv1 = findViewById(R.id.personal_item_tv1);//最上面  个人中心
        personalItemTv2 = findViewById(R.id.personal_item_tv2);//充值记录
        personalItemTv3 = findViewById(R.id.personal_item_tv3);//阈值设置

        dongHuaTu = findViewById(R.id.img_cursor); //上面那个跟随滑动  动的图片
        personalViewPager = findViewById(R.id.personal_viewPager);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.personal_item_tv1:personalViewPager.setCurrentItem(0);break;
            case R.id.personal_item_tv2:personalViewPager.setCurrentItem(1);break;
            case R.id.personal_item_tv3:personalViewPager.setCurrentItem(2);break;
            case R.id.imageView_back   :                           finish();break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private class MyPageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Animation animation = null;
            switch (position) {
                case 0:
                    if (currIndex == 1) {
                        animation = new TranslateAnimation(one, 0, 0, 0);
                    } else if (currIndex == 2) {
                        animation = new TranslateAnimation(two, 0, 0, 0);
                    }
                    break;
                case 1:
                    if (currIndex == 0) {
                        animation = new TranslateAnimation(0, one, 0, 0);
                    } else if (currIndex == 2) {
                        animation = new TranslateAnimation(two, one, 0, 0);
                    }
                    break;
                case 2:
                    if (currIndex == 0) {
                        animation = new TranslateAnimation(0, two, 0, 0);
                    } else if (currIndex == 1) {
                        animation = new TranslateAnimation(one, two, 0, 0);
                    }
                    break;
            }// switch
            currIndex = position;
            animation.setFillAfter(true);// true表示图片停在动画结束位置
            animation.setDuration(300); //设置动画时间为300毫秒
            dongHuaTu.startAnimation(animation);//开始动画
        }// public void onPageSelected

        @Override
        public void onPageScrollStateChanged(int state) {
        }

    }//private class MyPageChangeListener

}//class
