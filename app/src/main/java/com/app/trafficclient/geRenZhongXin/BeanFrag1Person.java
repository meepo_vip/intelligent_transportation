package com.app.trafficclient.geRenZhongXin;

public class BeanFrag1Person {

    /**
     * password : 123456
     * statusInfo : 请求成功
     * phone : 13804110001
     * sex : 男
     * registered : 1547913600000
     * id : 370101196101011001
     * account : user1
     * status : 1
     * username : 章敬泉
     */
    private String password;
    private String statusInfo;
    private String phone;
    private String sex;
    private long registered;
    private String id;
    private String account;
    private int status;
    private String username;

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setRegistered(long registered) {
        this.registered = registered;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public String getPhone() {
        return phone;
    }

    public String getSex() {
        return sex;
    }

    public long getRegistered() {
        return registered;
    }

    public String getId() {
        return id;
    }

    public String getAccount() {
        return account;
    }

    public int getStatus() {
        return status;
    }

    public String getUsername() {
        return username;
    }
}
