package com.app.trafficclient.dianChuanGanQiTiaoZhuanDeZheXianTu;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.trafficclient.R;

import java.util.ArrayList;
import java.util.List;

public class ZheXianTu extends AppCompatActivity implements
        FragmentShiDu.OnFragmentInteractionListener
       ,FragmentWenDu.OnFragmentInteractionListener
       ,FragmentGuangZhao.OnFragmentInteractionListener
       ,FragmentCo2.OnFragmentInteractionListener
       ,FragmentPM25.OnFragmentInteractionListener
       ,FragmentDaoLu.OnFragmentInteractionListener{

//    这是 一个viewpager +  Fragment
    private ViewPager zhexianPager;
    MyFragPagerAdapter myFragPagerAdapter;
    private RadioGroup zheXianRgp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhe_xian_tu);
        zhexianPager = findViewById(R.id.zhexian_pager);
        zheXianRgp = findViewById(R.id.zheXian_rgp);

        Intent intent = getIntent();
        int index = intent.getIntExtra("index",0);


        List<Fragment> list =new ArrayList<>();
        list.add(new FragmentWenDu());
        list.add(new FragmentShiDu());
        list.add(new FragmentGuangZhao());
        list.add(new FragmentCo2());
        list.add(new FragmentPM25());
        list.add(new FragmentDaoLu());

         myFragPagerAdapter = new MyFragPagerAdapter(getSupportFragmentManager(),list);
//        zhexianPager.setCurrentItem(3);  // 一定在Adapter之后调用
        zhexianPager.setAdapter(myFragPagerAdapter);
        zhexianPager.setCurrentItem(index);
        ((RadioButton)zheXianRgp.getChildAt(index)).setChecked(true);
        zhexianPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i=0;i<6;i++){
                    ((RadioButton)zheXianRgp.getChildAt(i)).setChecked(i == position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
