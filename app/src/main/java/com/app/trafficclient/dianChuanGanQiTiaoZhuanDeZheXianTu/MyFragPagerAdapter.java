package com.app.trafficclient.dianChuanGanQiTiaoZhuanDeZheXianTu;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MyFragPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> list = new ArrayList<>();
    public MyFragPagerAdapter(FragmentManager fm,List<Fragment> list1) {
        super(fm);
        this.list = list1;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
