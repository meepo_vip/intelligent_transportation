package com.app.trafficclient.dianChuanGanQiTiaoZhuanDeZheXianTu;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.trafficclient.R;
import com.app.trafficclient.MPAndroidChart.LiChart;
import com.app.trafficclient.util.UserDao;
import com.github.mikephil.charting.charts.LineChart;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDaoLu.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDaoLu#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDaoLu extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentDaoLu() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDaoLu.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDaoLu newInstance(String param1, String param2) {
        FragmentDaoLu fragment = new FragmentDaoLu();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private LineChart lineChart;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragment_dao_lu, container, false);
      lineChart= view.findViewById(R.id.frag_daolu);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        UserDao dao=new UserDao(getContext());
        Cursor cursor = dao.getEnvironment();
        List<Integer> list = new ArrayList();
        List<String> name=new ArrayList<>();
        List<String> lable=new ArrayList<>();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("mm:ss");
        lable.add("道路情况");

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int co2 =  cursor.getInt(cursor.getColumnIndex("roadStatus") );
            list.add(co2); //遍历cursor,把温度的值，放进list
            name.add(simpleDateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex("time")))));
        }
        List<List> val=new ArrayList<>();
        val.add(list);
        List<Integer> color=new ArrayList<>();
        color.add(getResources().getColor(R.color.listfont));
        LiChart.setLineChart(val,name,lable,color,lineChart,"道路状况折线图");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
