package com.app.trafficclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.app.trafficclient.login.LoginActivity;


public class GuideActivity extends Activity {

	RelativeLayout guide_RL;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//为了全屏显示,进；进来加载的图片
		requestWindowFeature(Window.FEATURE_NO_TITLE);//隐藏标题栏，写在setContentView前否则失效
		//去掉系统任务栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_guide);
		guide_RL = (RelativeLayout) findViewById(R.id.guide_RL);
		guide_RL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(),LoginActivity.class));
				finish();
			}
		});

	}//oncreate


}
