package com.app.trafficclient;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trafficclient.util.UserDao;

import java.text.SimpleDateFormat;
import java.util.Date;


public class zhangDanGuanli extends AppCompatActivity {

    TableLayout tableLayout;
    LinearLayout linearLayout;

    Button sel;
    Spinner spinner;//升降序
    UserDao dao;
    int sumRow;  //记录数  设置行数
    TextView title;
    ImageView back;
    LinearLayout.LayoutParams tbr_1;
    LinearLayout.LayoutParams all_1;

    Date dates; //格化式时间
    int width;  //屏幕宽
    int flag=0;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年-MM月dd日-HH:mm:ss");
    String[] tableTitleData ={"序号","车号","充值金额(元)","操作人","充值时间"};
    String[] tableData =new String[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bill_manage);
        initView();
        initAscData();
        initListen();

    }

    private void initListen() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tableLayout.removeAllViews();
                if (flag == 0){ //选的spinner的第0项
                    initAscData();  //正序 遍历
                }else {
                    initDescData(); //倒序
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                flag=position; //吧第几项给他
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }//initLinten

    private void initAscData() {

        Display display = getWindowManager().getDefaultDisplay();
         width = display.getWidth();
        sumRow = dao.getSumRecord();//返回多少条记录(int) 用来生成多少行
        if (sumRow == 0){ //如果返回的是0 ，就说明没有数据
            ifNull();
        }else {
            setTopRow(tableTitleData);//设置第一行 文字行
            Cursor cursor = dao.getAllRecord();
            cursor.moveToFirst(); //1
            for (int i = 0;i<sumRow;i++) {
                String carNum = cursor.getString(cursor.getColumnIndex("carNumber"));
                int money = cursor.getInt(cursor.getColumnIndex("money"));
                String people  = cursor.getString(cursor.getColumnIndex("people"));//此句改成  获取登陆的账号
                long time = cursor.getLong(cursor.getColumnIndex("time"));
                dates = new Date(time);

                cursor.moveToNext();//向下
                tableData[0] = String.valueOf(i+1);
                tableData[1] = carNum;
                tableData[2] = String.valueOf(money);
                tableData[3] = String.valueOf(people);
                tableData[4] = formatter.format(dates);
                setTopRow(tableData); //把数据传进去，设置行的内容
            }//for结束
        }//else
    }//initAscDesc

//如果记录为空调用
    private void ifNull() {
        //new 文本框
        TextView showDia = new TextView(getApplicationContext());
//        设置色白色
        showDia.setBackgroundColor(Color.WHITE);
        showDia.setText("暂无历史记录");
//        吧文本框添加到table
        tableLayout.addView(showDia);
        return;
    }

    //倒序
    private void initDescData() {
        if (sumRow == 0){
          ifNull();
        }else {
            setTopRow(tableTitleData);//设置第一行 文字行
            Cursor cursor = dao.getAllRecord();
            cursor.moveToLast(); //最后
            for (int i = 0;i<sumRow;i++) {
                String carNum = cursor.getString(cursor.getColumnIndex("carNumber"));
                int money = cursor.getInt(cursor.getColumnIndex("money"));
                String people  = cursor.getString(cursor.getColumnIndex("people"));//此句改成  获取登陆的账号
                long time = cursor.getLong(cursor.getColumnIndex("time"));
                dates = new Date(time);

                cursor.moveToPrevious();//向上
                tableData[0] = String.valueOf(i+1);
                tableData[1] = carNum;
                tableData[2] = String.valueOf(money);
                tableData[3] = String.valueOf(people);
                tableData[4] = formatter.format(dates);
                setTopRow(tableData);
            }//for(i)
        }//else
    } //initDescData
    private void initView() {
        linearLayout = findViewById(R.id.bill_linner);
        tableLayout = findViewById(R.id.bill_tableout);
        sel = findViewById(R.id.bill_btn_sele);
        spinner = findViewById(R.id.bill_spinner);
        title = findViewById(R.id.tv_title);
        title.setText("账单管理");
        back = findViewById(R.id.imageView_back);
        back.setImageResource(R.drawable.change);
        dao =new UserDao(getApplicationContext());

        tbr_1 = new TableRow.LayoutParams();
        tbr_1.setMargins(0,1,1,1);

        all_1 = new TableRow.LayoutParams();
        all_1.setMargins(1,1,1,1);

   }


   public  void setTopRow(String tableData[]){
        //表格布局的行
    TableRow row = new TableRow(getApplicationContext());
    for (int i = 0; i <5; i++) {   //5列
        TextView txvTop = new TextView(getApplicationContext());
        //居中
        txvTop.setGravity(Gravity.CENTER);
        txvTop.setTextSize(18);
        txvTop.setTextColor(getResources().getColor(R.color.heiSe));
        txvTop.setBackgroundColor(Color.WHITE);
        switch (i) {         //这只文本               宽                                    格式  上面写了
            case 0: txvTop.setText(tableData[i]);txvTop.setWidth((int)(0.069 * width));txvTop.setLayoutParams(all_1);break;
            case 1: txvTop.setText(tableData[i]);txvTop.setWidth((int)(0.069 * width));txvTop.setLayoutParams(tbr_1);break;
            case 2: txvTop.setText(tableData[i]);txvTop.setWidth((int)(0.162 * width));txvTop.setLayoutParams(tbr_1);break;
            case 3: txvTop.setText(tableData[i]);txvTop.setWidth((int)(0.125 * width));txvTop.setLayoutParams(tbr_1);break;
            case 4: txvTop.setText(tableData[i]);txvTop.setWidth((int)(0.371 * width));txvTop.setLayoutParams(tbr_1);break;
            default: break;
        }//switch
        row.addView(txvTop);
    }//for(i)
    tableLayout.addView(row);
   }//setRow

}//class
