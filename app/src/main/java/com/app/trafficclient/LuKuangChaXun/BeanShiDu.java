package com.app.trafficclient.LuKuangChaXun;

public class BeanShiDu {

    /**
     * UserName : user1
     * SenseName : temperature
     */
    private String UserName;
    private String SenseName;

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public void setSenseName(String SenseName) {
        this.SenseName = SenseName;
    }

    public String getUserName() {
        return UserName;
    }

    public String getSenseName() {
        return SenseName;
    }
}
