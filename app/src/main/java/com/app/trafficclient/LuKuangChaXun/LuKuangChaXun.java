package com.app.trafficclient.LuKuangChaXun;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.trafficclient.R;
import com.app.trafficclient.ZhangHuGuanli.MyDialog;
import com.app.trafficclient.util.MyLoadDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LuKuangChaXun extends AppCompatActivity {
    private ImageButton luKuangImbShuaXin;
    private TextView luKuangTxvRiQi;
    private TextView luKuangTxvXingQi;
    private TextView luKuangTxvWenDu;
    private TextView luKuangTxvShiDu;
    private TextView luKuangTxvPm2;
    private ImageView luKuangImgJiaoJingNan;
    private ImageView luKuangImgJiaoJingNv;



    Context context;
    View[] view;
    int [] colors;
    int [] suiji;
   Handler handler;
   Handler handler1;
   MyLoadDialog myDialog;
   boolean flag = true;
   RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lu_kuang_cha_xun);
        initView();
        initTimer();
        initTimer1();
        initdata();
        initListener();
    }

    private void initListener() {
        luKuangImbShuaXin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPost();
            }
        });
    }

    private void sendPost() {
        //这个接口有问题！同理，自己写
       String url ="";
    }

    private void initdata() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d");
        SimpleDateFormat format1 = new SimpleDateFormat("EEEE");
        Date date = new Date(System.currentTimeMillis());
        String riQi = format.format(date);
        String xingQi = format1.format(date);
        luKuangTxvRiQi.setText(riQi);
        luKuangTxvXingQi.setText(xingQi);
    }

    private void initTimer1() {
        handler1 = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1: luKuangImgJiaoJingNan.setBackgroundDrawable(getResources().getDrawable(R.drawable.jiaojing1_1));
                            luKuangImgJiaoJingNv.setBackgroundDrawable(getResources().getDrawable(R.drawable.jiaojing2_1));
                            break;
                    case 2: luKuangImgJiaoJingNan.setBackgroundDrawable(getResources().getDrawable(R.drawable.jiaojing1_2));
                            luKuangImgJiaoJingNv.setBackgroundDrawable(getResources().getDrawable(R.drawable.jiaojing2_2));
                            break;
                }
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (flag){
                    handler1.sendEmptyMessage(1);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler1.sendEmptyMessage(2);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    @Override
    protected void onDestroy() {
        flag = false;
        super.onDestroy();
    }

    private void initTimer() {
        showMyDialog();
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
//                super.handleMessage(msg);
                myDialogDismiss();
                for (int i = 0;i<9;i++) {
                    suiji[i] = (int) (Math.random() * 5);
                    if (i<3){
                        view[i].setBackgroundColor(colors[suiji[0]]);
                    }else {
                        view[i].setBackgroundColor(colors[suiji[i]]);
                    }
                    Log.d("taggg", suiji[i]+"  ");
                }


            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
              while (flag){
                  try {
                      Thread.sleep(3000);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
                  handler.sendEmptyMessage(1);

              }
            }
        }).start();
    }

    private void myDialogDismiss() {
        myDialog.dismiss();
    }

    private void showMyDialog() {
        myDialog =new MyLoadDialog(context);
        myDialog.setCanceledOnTouchOutside(false);
        myDialog.show();
    }

    private void initView() {
        context = this;
        requestQueue = Volley.newRequestQueue(context);
        view = new View[9];
        colors = new int[]{getResources().getColor(R.color.yanzhong),getResources().getColor(R.color.zhongdu),
                getResources().getColor(R.color.yiban),getResources().getColor(R.color.huanxing),
                getResources().getColor(R.color.changtong)};
        suiji = new int[9];


        luKuangImgJiaoJingNan = findViewById(R.id.luKuang_img_jiaoJingNan);
        luKuangImgJiaoJingNv = findViewById(R.id.luKuang_img_jiaoJingNv);

        view[0] = findViewById(R.id.luKuang_huangChengKuaiSu_L);
        view[1] = findViewById(R.id.luKuang_huangChengKuaiSu_T);
        view[2] = findViewById(R.id.luKuang_huangChengKuaiSu_B);

        view[3] = findViewById(R.id.luKuang_huangChengGaoSu);
        view[4] = findViewById(R.id.luKuang_xueYuanLu);
        view[5] = findViewById(R.id.luKuang_lianXiangLu);
        view[6] = findViewById(R.id.luKuang_xingFuLu);
        view[7] = findViewById(R.id.luKuang_yiYuanLu);
        view[8] = findViewById(R.id.luKuang_tingChe);

        luKuangImbShuaXin = findViewById(R.id.luKuang_imb_shuaXin);
        luKuangTxvRiQi = findViewById(R.id.luKuang_txv_riQi);
        luKuangTxvXingQi = findViewById(R.id.luKuang_txv_xingQi);
        luKuangTxvWenDu = findViewById(R.id.luKuang_txv_wenDu);
        luKuangTxvShiDu = findViewById(R.id.luKuang_txv_shiDu);
        luKuangTxvPm2 = findViewById(R.id.luKuang_txv_pm2);
    }
}
