package com.app.trafficclient.WoDeZuoJia;

import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trafficclient.R;

import java.util.ArrayList;
import java.util.List;

public class WoDeZuoJia extends AppCompatActivity implements FragmentWoDeYuE.OnFragmentInteractionListener,
FragmentYuanChengKongZhi.OnFragmentInteractionListener,FragmentChongZhiJiLu.OnFragmentInteractionListener, View.OnClickListener{
    private LinearLayout linearTittle;
    private ImageView imageViewBack;
    private TextView tvTitle;
    private ViewPager zuoJiaViewPager;
    private TextView zuoJiaTvDangQianZhuangHu;
    private TextView zuoJiaTvWoDeYuE;
    private TextView zuoJiaTvYuanChengKongZhi;
    private TextView zuoJiaTvChongZhiJiLu;
    List<Fragment> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wo_de_zuo_jia);
        initUI();
        initData();
        initListener();

    }

    private void initListener() {
        zuoJiaViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
              jiaCu(position);
//                Toast.makeText(getApplicationContext(),"ok",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        zuoJiaTvWoDeYuE.setOnClickListener(this);
        zuoJiaTvYuanChengKongZhi.setOnClickListener(this);
        zuoJiaTvChongZhiJiLu.setOnClickListener(this);
    }

    private void jiaCu(int position) {
        switch (position) {
            case 0:zuoJiaTvWoDeYuE.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
                   zuoJiaTvYuanChengKongZhi.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);
                   zuoJiaTvChongZhiJiLu.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);break;
            case 1:zuoJiaTvWoDeYuE.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);
                zuoJiaTvYuanChengKongZhi.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
                zuoJiaTvChongZhiJiLu.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);break;
            case 2:zuoJiaTvWoDeYuE.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);
                zuoJiaTvYuanChengKongZhi.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);
                zuoJiaTvChongZhiJiLu.setTypeface(Typeface.DEFAULT,Typeface.BOLD);break;
        }
    }

    private void initData() {
        list = new ArrayList<>();
        list.add(new FragmentWoDeYuE());
        list.add(new FragmentYuanChengKongZhi());
        list.add(new FragmentChongZhiJiLu());

        Adapter adapter = new Adapter(getSupportFragmentManager(),list);
        zuoJiaViewPager.setAdapter(adapter);
        zuoJiaViewPager.setCurrentItem(0);
        zuoJiaViewPager.setOffscreenPageLimit(2);

    }

    private void initUI() {
        linearTittle = findViewById(R.id.linearTittle);
        imageViewBack = findViewById(R.id.imageView_back);
        tvTitle = findViewById(R.id.tv_title);
        zuoJiaViewPager = findViewById(R.id.zuoJia_viewPager);
        zuoJiaTvDangQianZhuangHu = findViewById(R.id.zuoJia_tv_dangQianZhuangHu);
        zuoJiaTvWoDeYuE = findViewById(R.id.zuoJia_tv_woDeYuE);
        zuoJiaTvYuanChengKongZhi = findViewById(R.id.zuoJia_tv_yuanChengKongZhi);
        zuoJiaTvChongZhiJiLu = findViewById(R.id.zuoJia_tv_chongZhiJiLu);
        zuoJiaTvWoDeYuE.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.zuoJia_tv_woDeYuE:zuoJiaViewPager.setCurrentItem(0);break;
            case R.id.zuoJia_tv_yuanChengKongZhi:zuoJiaViewPager.setCurrentItem(1);break;
            case R.id.zuoJia_tv_chongZhiJiLu:zuoJiaViewPager.setCurrentItem(2);break;
        }




    }
}
