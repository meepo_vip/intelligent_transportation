package com.app.trafficclient.WoDeZuoJia;

public class BeanYuanChengZhuangTai {

    /**
     * ERRMSG : 成功
     * RESULT : S
     */
    private String ERRMSG;
    private String RESULT;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public String getRESULT() {
        return RESULT;
    }
}
