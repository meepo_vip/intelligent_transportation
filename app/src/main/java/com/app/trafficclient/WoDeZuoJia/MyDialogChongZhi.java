package com.app.trafficclient.WoDeZuoJia;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.app.trafficclient.R;
import com.app.trafficclient.util.GetUrl;
import com.app.trafficclient.util.MyLoadDialog;
import com.app.trafficclient.util.UserDao;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyDialogChongZhi extends Dialog implements View.OnClickListener {

    private EditText fragDialogEdt;
    private Button fragDialogBtnOk;
    private Button fragDialogBtnCancel;
    private ImageButton fragDialogImgCancel;
     int carId;
    RequestQueue requestQueue;
    MyLoadDialog myLoadDialog;
    UserDao dao;
    long time;


    public MyDialogChongZhi( Context context,int i) {
        super(context);
        setContentView(R.layout.zuo_jia_mydialog_chong_zhi);
        carId =i;
        initView();
        initListener();
    }

    private void initView() {
        fragDialogEdt = findViewById(R.id.frag_dialog_edt);
        fragDialogBtnOk = findViewById(R.id.frag_dialog_btn_ok);
        fragDialogBtnCancel = findViewById(R.id.frag_dialog_btn_cancel);
        fragDialogImgCancel = findViewById(R.id.frag_dialog_img_cancel);
        dao = new UserDao(getContext());

    }

    private void initListener() {
        fragDialogBtnOk.setOnClickListener(this);
        fragDialogImgCancel.setOnClickListener(this);
        fragDialogBtnCancel.setOnClickListener(this);
        requestQueue = Volley.newRequestQueue(getContext());
        myLoadDialog = new MyLoadDialog(getContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.frag_dialog_img_cancel:this.dismiss();break;
            case  R.id.frag_dialog_btn_cancel:this.dismiss();break;
            case  R.id.frag_dialog_btn_ok:
                if(fragDialogEdt.getText().length()==0 || Integer.parseInt(fragDialogEdt.getText().toString().trim())==0){
                    Toast.makeText(getContext(),"请核对金额！",Toast.LENGTH_SHORT).show();break; }
                sendPost();break;
        }
    }

    private  void sendPost() {
       MyLoadDialog.showDia();
        String url = GetUrl.getUrl()+"action/SetCarAccountRecharge.do";
        Map<String,Object> map = new HashMap<>();
        final int money = Integer.parseInt(fragDialogEdt.getText().toString().trim());
        time = System.currentTimeMillis();

        map.put("CarId",carId);
        map.put("Money",money);
        map.put("UserName","user1");  //改成登陆的账户
        JSONObject jsonObject = new JSONObject(map);
        JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                String string = jsonObject.toString();
                Gson gson = new Gson();
                BeanChongZhiZhuangTai beanChongZhiZhuangTai = gson.fromJson(string,BeanChongZhiZhuangTai.class);
                if (beanChongZhiZhuangTai.getERRMSG().equals("成功")){
                    Toast.makeText(getContext(),carId+"号小车充值"+fragDialogEdt.getText().toString().trim()
                            +"元成功!",Toast.LENGTH_LONG).show();
                    dao.ins(String.valueOf(carId),money,"user1",time);
                }else {
                    Toast.makeText(getContext(),carId+"号小车充值失败!",Toast.LENGTH_LONG).show();
                }
                MyLoadDialog.disDia();
                MyDialogChongZhi.this.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getContext(),carId+"号小车充值失败!",Toast.LENGTH_LONG).show();
                MyLoadDialog.disDia();
                MyDialogChongZhi.this.dismiss();
            }
        });
        requestQueue.add(jsonRequest);
    }

}
