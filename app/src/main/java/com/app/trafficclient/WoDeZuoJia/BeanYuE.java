package com.app.trafficclient.WoDeZuoJia;

public class BeanYuE {

    /**
     * ERRMSG : 成功
     * RESULT : S
     * Balance : 966
     */
    private String ERRMSG;
    private String RESULT;
    private int Balance;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public void setBalance(int Balance) {
        this.Balance = Balance;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public String getRESULT() {
        return RESULT;
    }

    public int getBalance() {
        return Balance;
    }
}
