package com.app.trafficclient.WoDeZuoJia;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.trafficclient.R;
import com.app.trafficclient.util.UserDao;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentChongZhiJiLu.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentChongZhiJiLu#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentChongZhiJiLu extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentChongZhiJiLu() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentChongZhiJiLu.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChongZhiJiLu newInstance(String param1, String param2) {
        FragmentChongZhiJiLu fragment = new FragmentChongZhiJiLu();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private LinearLayout chongZhiJiLuLinerAdd;
    UserDao dao;
    Context context;
    LinearLayout.LayoutParams layoutParamsBUJU;
    LinearLayout.LayoutParams layoutParamsXUHAO;
    LinearLayout.LayoutParams layoutParamsBIANHAO;
    LinearLayout.LayoutParams layoutParamsSHUE;
    LinearLayout.LayoutParams layoutParamsRIQI;
    String[] data= new String[3];
    int rows;
    Date date;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
    static  int i=1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_chong_zhi, container, false);
        chongZhiJiLuLinerAdd = (LinearLayout) view.findViewById(R.id.chongZhiJiLu_Liner_Add);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        context =getContext();
        dao = new UserDao(getContext());
        initLayoutParams();
        initData();

    }

    private void initLayoutParams() {
        layoutParamsBUJU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsBUJU.setMargins(0,20,0,0);

        layoutParamsXUHAO = new LinearLayout.LayoutParams(100,LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsXUHAO.setMargins(100,0,0,0);

        layoutParamsBIANHAO = new LinearLayout.LayoutParams(120,LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsBIANHAO.setMargins(100,0,0,0);;

        layoutParamsSHUE = new LinearLayout.LayoutParams(200,LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsSHUE.setMargins(100,0,0,0);

        layoutParamsRIQI = new LinearLayout.LayoutParams(400,LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsRIQI.setMargins(100,0,0,0);

    }

    private void initData() {
         rows = dao.getSumRecord();
        if (rows==0){
            chongZhiJiLuLinerAdd.setGravity(Gravity.CENTER);
            TextView textView = new TextView(context);
            textView.setText("目前暂无充值记录");
            textView.setTextSize(30);
            chongZhiJiLuLinerAdd.addView(textView);
        }else {
            Cursor cursor = dao.getAllRecord();
            cursor.moveToLast();
            for (int j= 0;j<rows;j++){
                String cheHao = cursor.getString(cursor.getColumnIndex("carNumber"));
                String jinE = String.valueOf(  cursor.getInt(cursor.getColumnIndex("money"))  );
               long  longRiQi = cursor.getLong(cursor.getColumnIndex("time"));
               date = new Date(longRiQi);
               String riQi = simpleDateFormat.format(date);

               data[0] = cheHao;
               data[1] =jinE;
               data[2] = riQi;
                Log.d("carnumber",data[0]+"   "+data[1]+"   "+data[2]);
               setRowData(data);
               cursor.moveToPrevious();
            }

        }
    }

    private void setRowData(String[] data) {


           LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(layoutParamsBUJU);

            TextView textView1 = new TextView(context);
            textView1.setText(String.valueOf(i));
            textView1.setTextSize(25);
            textView1.setGravity(Gravity.CENTER);
            textView1.setLayoutParams(layoutParamsXUHAO);

            TextView textView2 = new TextView(context);
            textView2.setText(data[0]);
            textView2.setTextSize(25);
            textView2.setGravity(Gravity.CENTER);
            textView2.setLayoutParams(layoutParamsBIANHAO);

            TextView textView3 = new TextView(context);
            textView3.setText(data[1]);
            textView3.setTextSize(25);
            textView3.setGravity(Gravity.CENTER);
            textView3.setLayoutParams(layoutParamsSHUE);

            TextView textView4 = new TextView(context);
            textView4.setText(data[2]);
            textView4.setTextSize(25);
            textView4.setGravity(Gravity.CENTER);
            textView4.setLayoutParams(layoutParamsRIQI);
            linearLayout.addView(textView1);
            linearLayout.addView(textView2);
            linearLayout.addView(textView3);
            linearLayout.addView(textView4);
        chongZhiJiLuLinerAdd.addView(linearLayout);
        i++;
        
    }


    @Override
    public void onStop() {
        super.onStop();
        chongZhiJiLuLinerAdd.removeAllViews();
        i =1;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
