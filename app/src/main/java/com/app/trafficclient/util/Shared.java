package com.app.trafficclient.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Shared {
    public static SharedPreferences get(Context context){
        return context.getSharedPreferences("traffic",Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor edit(Context context){
        return get(context).edit();
    }
}
