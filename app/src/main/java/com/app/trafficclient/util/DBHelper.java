package com.app.trafficclient.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, "traffic.db", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                " create table recharge(_id Integer primary key autoincrement, "
                        + " time long, "
                        + " people text, "
                        + " carNumber text, "
                        + " money int) "
        );
        db.execSQL("create table AllSense" +
                "(temperature int," +
                "humidity int," +
                "LightIntensity int," +
                "co2 int," +
                "pm2_5 int," +
                "roadStatus int," +
                "time long)");

//        db.execSQL();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
