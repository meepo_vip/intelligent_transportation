package com.app.trafficclient.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.inputmethodservice.Keyboard;

import java.sql.RowId;


public class UserDao {

    DBHelper dbHelper;

    SQLiteDatabase db;
    ContentValues values;

    public UserDao(Context context) {
        dbHelper = new DBHelper(context);
    }



//储存充值记录
    public long ins(String carNum,int money,String peo,long time){
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put("carNumber",carNum);
        values.put("money",money);
        values.put("people",peo);
        values.put("time",time);
        long flag= db.insert("recharge",null,values);
        db.close();
        return  flag;
    }

    //根据车号返回的是该表中的cursor，
//    public Cursor sel(String carNum){
//        db = dbHelper.getWritableDatabase();
//        Cursor cursor = db.query("recharge",null,"carNumber = ?",new String[]{String.valueOf(carNum)},
//                null,null,null);
//        if (cursor.getCount() == 0){ //如果没有记录
//            return null;
//        }
//        //db.close();
//        return cursor;
//    }

    //返回总记录数，后面账单管理和个人中心,显示多少行表格
public  int getSumRecord(){
        int row=0;
       db = dbHelper.getWritableDatabase();
       Cursor cursor = db.query("recharge",null,null,null,
            null,null,null);
       row = cursor.getCount();
       cursor.close();
       db.close();
       return row;
}

//返回的是充值记录  cursor ，用于填充，账单管理的表格和个人中心的充值记录
public Cursor getAllRecord(){
    db = dbHelper.getWritableDatabase();
    Cursor cursor = db.query("recharge",null,null,null,
            null,null,null);
    return cursor;
}


//环境指标 6个传感器信息存储
public int Environment(int temperature,int humidity,int LightIntensity,
                        int co2 ,int pm2_5,int roadStatus ){

    db = dbHelper.getWritableDatabase();
    values = new ContentValues();

    values.put("temperature",temperature);
    values.put("humidity",humidity);
    values.put("LightIntensity",LightIntensity);
    values.put("co2",co2);
    values.put("pm2_5", pm2_5);
    values.put("roadStatus",roadStatus);
    values.put("time",System.currentTimeMillis());

    db.insert("AllSense",null,values);
    Cursor cursor = db.query("AllSense",null,null,
            null, null,null,null);
    int i=cursor.getCount();
//大于20条数据，删除时间最早的那个;
 if(i>20){
     db.execSQL("delete  from AllSense where time = (select min(time) from AllSense)");
   }
    cursor.close();

    db.close();

return  i-1;
}

//环境指标的cursor
    public Cursor getEnvironment(){
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query("AllSense",null,null,null,
                null,null,null);
        return cursor;
    }


}
