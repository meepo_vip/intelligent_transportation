package com.app.trafficclient.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * 项目名称：
 * 类描述：
 * 创建人：zhaowei
 * 创建时间：2017/4/19 9:20
 * 修改人：Administrator
 * 修改时间：2017/4/19 9:20
 * 修改备注：
 */
public class Util {

    public static String urlHttp;
    public static String urlPort ;

    /**
     * 描述：保存数据到SharedPreferences对象中
     * @param ipUrl
     * @param ipPort
     */

    public static void saveSetting(String ipUrl, String ipPort, Context context) {
        // 将需要记录的数据保存在setting.xml文件中
  /**  第一个参数：  name为本组件的配置文件名(自定义)
        第二个参数：mode指定为MODE_PRIVATE，则该配置文件只能被自己的应用程序访问。(也可写成0)*/
        SharedPreferences spSettingSave = context.getSharedPreferences("setting", MODE_PRIVATE);
        SharedPreferences.Editor editor = spSettingSave.edit();//获取Editor对象（由SharedPreferences对象调用）
        editor.putString("ipUrl", ipUrl);//写入String类型的数据,键值对
        editor.putString("ipPort", ipPort);
        editor.commit();//提交修改
    }

    /**
     * 描述：获取数据到SharedPreferences对象中
     * @return
     */
    public static UrlBean loadSetting(Context context) {
        UrlBean urlBean=new UrlBean();

        SharedPreferences loadSettingLoad = context.getSharedPreferences("setting", MODE_PRIVATE);
        //这里是将setting.xml 中的数据读出来  这个文件是上面以键值对保存的ip和端口的数据
        urlBean.setUrl( loadSettingLoad.getString("ipUrl", "") );  //从setting在 读 键ipurl，用urlBean的setUrl赋值
        urlBean.setPort( loadSettingLoad.getString("ipPort", "") );

//        String urlSetting = "http://" + urlHttp+ ":" + urlPort + "/";
        return urlBean;
    }
}
