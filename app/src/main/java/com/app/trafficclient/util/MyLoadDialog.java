package com.app.trafficclient.util;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.app.trafficclient.R;

public class MyLoadDialog extends Dialog {
    static MyLoadDialog myLoadDialog;
    public MyLoadDialog( Context context) {
        super(context);
        setContentView(R.layout.loading_dialog);
        myLoadDialog = this;
    }

    public static void showDia(){
        myLoadDialog.setCanceledOnTouchOutside(false);
        myLoadDialog.show();
    }

    public static void disDia(){
        myLoadDialog.dismiss();
    }



}
