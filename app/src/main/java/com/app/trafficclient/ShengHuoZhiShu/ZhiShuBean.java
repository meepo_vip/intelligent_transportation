package com.app.trafficclient.ShengHuoZhiShu;

public class ZhiShuBean {

    /**
     * statusInfo : 请求成功
     * light : 871
     * co2 : 4501
     * temperature : 9
     * humidity : 20
     * pm : 466
     * status : 1
     */
    private String statusInfo;
    private int light;
    private int co2;
    private int temperature;
    private int humidity;
    private int pm;
    private int status;

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public void setLight(int light) {
        this.light = light;
    }

    public void setCo2(int co2) {
        this.co2 = co2;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setPm(int pm) {
        this.pm = pm;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public int getLight() {
        return light;
    }

    public int getCo2() {
        return co2;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPm() {
        return pm;
    }

    public int getStatus() {
        return status;
    }
}
