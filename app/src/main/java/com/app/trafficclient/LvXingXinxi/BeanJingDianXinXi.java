package com.app.trafficclient.LvXingXinxi;

import java.util.List;

public class BeanJingDianXinXi {

    /**
     * ERRMSG : 成功
     * ROWS_DETAIL : [{"img":"img/002.png","ticket":80,"name":"故宫 ","rating":5,"tel":"010- 88888888","id":1,"info":"北京故宫是中国明清两代的皇家宫殿，旧 称为紫禁城，位于北京中轴线的中心，是中国古代宫廷建筑之精华。北京故宫以三大殿为 中心，占地面积 72 万平方米，建筑面积约 15 万平方米，有大小宫殿七十多座，房屋九千 余间。是世界上现存规模最大、保存最为完整的木质结构古建筑之一。"},"\u2026\u2026"]
     * RUSULT : S
     */
    private String ERRMSG;
    private List<ROWS_DETAILEntity> ROWS_DETAIL;
    private String RUSULT;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setROWS_DETAIL(List<ROWS_DETAILEntity> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }

    public void setRUSULT(String RUSULT) {
        this.RUSULT = RUSULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public List<ROWS_DETAILEntity> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public String getRUSULT() {
        return RUSULT;
    }

    public class ROWS_DETAILEntity {
        /**
         * img : img/002.png
         * ticket : 80
         * name : 故宫
         * rating : 5
         * tel : 010- 88888888
         * id : 1
         * info : 北京故宫是中国明清两代的皇家宫殿，旧 称为紫禁城，位于北京中轴线的中心，是中国古代宫廷建筑之精华。北京故宫以三大殿为 中心，占地面积 72 万平方米，建筑面积约 15 万平方米，有大小宫殿七十多座，房屋九千 余间。是世界上现存规模最大、保存最为完整的木质结构古建筑之一。
         */
        private String img;
        private int ticket;
        private String name;
        private int rating;
        private String tel;
        private int id;
        private String info;

        public void setImg(String img) {
            this.img = img;
        }

        public void setTicket(int ticket) {
            this.ticket = ticket;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getImg() {
            return img;
        }

        public int getTicket() {
            return ticket;
        }

        public String getName() {
            return name;
        }

        public int getRating() {
            return rating;
        }

        public String getTel() {
            return tel;
        }

        public int getId() {
            return id;
        }

        public String getInfo() {
            return info;
        }
    }
}
