package com.app.trafficclient.LvXingXinxi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.trafficclient.R;

public class XiangQingMoXing extends AppCompatActivity {
    private ImageView xiangQingImg;
    private TextView xiangQingJianJie;
    private TextView xiangQingDengJi;
    private TextView xiangQingDianHua;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xiang_qing_mo_xing);
        initView();
        Intent intent =getIntent();
        int flag = intent.getIntExtra("flag",0);
                xiangQingImg.setImageBitmap(LvXingXinXi.getBitmaps()[flag]);
                xiangQingJianJie.setText(LvXingXinXi.getJianJie()[flag]);
                xiangQingDengJi.setText(String.valueOf(LvXingXinXi.getDengJi()[flag]));
                xiangQingDianHua.setText(LvXingXinXi.getDianHua()[flag]);
        Log.d("tagflag",flag+"");
    }

    private void initView() {
        xiangQingImg = findViewById(R.id.xiangQing_img);
        xiangQingJianJie = findViewById(R.id.xiangQing_jianJie);
        xiangQingDengJi = findViewById(R.id.xiangQing_dengJi);
        xiangQingDianHua = findViewById(R.id.xiangQing_dianHua);
    }
}
