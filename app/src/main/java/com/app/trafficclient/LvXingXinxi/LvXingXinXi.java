package com.app.trafficclient.LvXingXinxi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.app.trafficclient.MainActivity;
import com.app.trafficclient.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LvXingXinXi extends AppCompatActivity  {
    private ImageView imageViewBack;
    private TextView tvTitle;
    private ImageView lvXingLTimg;
    private TextView lvXingLTtxvDiDian;
    private TextView lvXingLTtxvJinE;
    private ImageView lvXingRTimg;
    private TextView lvXingRTtxvDiDian;
    private TextView lvXingRTtxvJinE;
    private ImageView lvXingLBimg;
    private TextView lvXingLBtxvDiDian;
    private TextView lvXingLBtxvJinE;
    private ImageView lvXingRBimg;
    private TextView lvXingRBdiDian;
    private TextView lvXingRBjinE;

    TextView[]  textViewsDidian;
    ImageView[] imageViewsImg;
    TextView [] textViewsJinE;
    Context context;
    RequestQueue requestQueue ;
    static final Bitmap[] bitmaps = new Bitmap[4];
    static String jianJie[];
    static int dengJi[];
    static String dianHua[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lv_xing_xin_xi);
        initView();
        sendPost();
        initListener();
    }

    private void initListener() {
        for (int i = 0;i<4;i++) {
            final int finalI = i;
            imageViewsImg[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentNew(finalI);
                }
            });
        }

    }//方法

    private void sendPost() {
        String url = "http://192.168.131.1:8088/transportservice/action/GetSpotInfo.do";
        Map<String, String> map = new HashMap<>();
        map.put("UserName","user1");
        JSONObject jsonObject = new JSONObject(map);
        JsonRequest<JSONObject> jsonObjectJsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                String string = jsonObject.toString();
                Log.d("url123",string);
                Gson gson =new Gson();
                BeanJingDianXinXi jingDianXinXi = gson.fromJson(string,BeanJingDianXinXi.class);
               for (int i= 0;i<jingDianXinXi.getROWS_DETAIL().size();i++){
                   final String name = jingDianXinXi.getROWS_DETAIL().get(i).getName();
                   final int piaoJia = jingDianXinXi.getROWS_DETAIL().get(i).getTicket();
                   String tuPian = jingDianXinXi.getROWS_DETAIL().get(i).getImg();
                   jianJie[i] = jingDianXinXi.getROWS_DETAIL().get(i).getInfo();
                   dianHua[i] = jingDianXinXi.getROWS_DETAIL().get(i).getTel();
                   dengJi[i] = jingDianXinXi.getROWS_DETAIL().get(i).getRating();
                   String imgUrl = "http://192.168.131.1:8088/transportservice"+tuPian;
                   Log.d("urllll",imgUrl);
                   final int finalI = i;
                   final int finalI1 = i;
                   ImageRequest imageRequest = new ImageRequest(imgUrl, new Response.Listener<Bitmap>() {
                       @Override
                       public void onResponse(Bitmap bitmap) {
                            bitmaps[finalI] = bitmap;
                           textViewsDidian[finalI1].setText(name);
                           textViewsJinE[finalI1].setText(String.valueOf(piaoJia));
                           imageViewsImg[finalI1].setImageBitmap(bitmaps[finalI1]);
                       }
                   }, 400, 400, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError volleyError) {
                          Log.d("utl123Err",volleyError.toString());
                       }
                   }); // 图片请求
                   requestQueue.add(imageRequest);
               } //for

            }//onResponse
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("url1234",volleyError.toString());
            }
        });
        requestQueue.add(jsonObjectJsonRequest);
    }//sendPOst

    public ImageView getImageViewBack() {
        return imageViewBack;
    }

    public static Bitmap[] getBitmaps() {
        return bitmaps;
    }

    public static String[] getJianJie() {
        return jianJie;
    }

    public static int[] getDengJi() {
        return dengJi;
    }

    public static String[] getDianHua() {
        return dianHua;
    }

    private void initView() {
        context = this;
        requestQueue = Volley.newRequestQueue(context);

        imageViewBack = findViewById(R.id.imageView_back);
        tvTitle = findViewById(R.id.tv_title);

        lvXingLTimg = findViewById(R.id.lvXing_LTimg);
        lvXingLTtxvDiDian = findViewById(R.id.lvXing_LTtxv_diDian);
        lvXingLTtxvJinE = findViewById(R.id.lvXing_LTtxv_jinE);

        lvXingRTimg = findViewById(R.id.lvXing_RTimg);
        lvXingRTtxvDiDian = findViewById(R.id.lvXing_RTtxv_diDian);
        lvXingRTtxvJinE = findViewById(R.id.lvXing_RTtxv_jinE);

        lvXingLBimg = findViewById(R.id.lvXing_LBimg);
        lvXingLBtxvDiDian = findViewById(R.id.lvXing_LBtxv_diDian);
        lvXingLBtxvJinE = findViewById(R.id.lvXing_LBtxv_jinE);

        lvXingRBimg = findViewById(R.id.lvXing_RBimg);
        lvXingRBdiDian = findViewById(R.id.lvXing_RBdiDian);
        lvXingRBjinE = findViewById(R.id.lvXing_RBjinE);

        textViewsDidian = new TextView[]{lvXingLTtxvDiDian,lvXingRTtxvDiDian,lvXingLBtxvDiDian,lvXingRBdiDian};
        textViewsJinE = new TextView[]{lvXingLTtxvJinE,lvXingRTtxvJinE,lvXingLBtxvJinE,lvXingRBjinE};
        imageViewsImg = new ImageView[]{lvXingLTimg,lvXingRTimg,lvXingLBimg,lvXingRBimg};

        jianJie = new String[4];
        dengJi = new int[4];
        dianHua = new String[4];
    }

    private void intentNew(int flag) {
        Log.d("tagflagsend",flag+"");
        Intent intent = new Intent();
        intent.setClass(context,XiangQingMoXing.class);
        intent.putExtra("flag",flag);
        startActivity(intent);
    }


}
