package com.app.trafficclient.woDeZhangHu;

public class yueBean {

    /**
     * statusInfo : 请求成功
     * balance : 256
     * id : 1
     * status : 1
     */
    private String statusInfo;
    private int balance;
    private int id;
    private int status;

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public int getBalance() {
        return balance;
    }

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }
}
