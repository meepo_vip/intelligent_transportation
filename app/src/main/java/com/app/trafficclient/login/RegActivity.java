package com.app.trafficclient.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.trafficclient.R;
import com.app.trafficclient.util.LoadingDialog;

public class RegActivity extends Activity {

	EditText accountET, pwdET, mspwdET,phone;
	Button regBtn,regback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reg);
		initView();
		initLsenter();
	}

	private void initView() {
		phone = (EditText) findViewById(R.id.reg_phone);
		accountET = (EditText) findViewById(R.id.accountET);
		pwdET = (EditText) findViewById(R.id.pwdET);
		mspwdET = (EditText) findViewById(R.id.mspwdET);
		regBtn = (Button) findViewById(R.id.regBtn);
		regback = (Button)findViewById(R.id.regback);
	}

	private void initLsenter() {
		regBtn.setOnClickListener(new View.OnClickListener() {
			String str;
			@Override
			public void onClick(View v) {
				if (accountET.length() == 0 || pwdET.length()==0 ||mspwdET.length()==0){
					str ="用户名和密码不能为空！";
					showdia(str);
				}
			   else if (pwdET.getText().toString().equals(mspwdET.getText().toString()) == false) {
					str = "两次密码不同！";
					showdia(str);
			   }
			      else if (phone.length() != 11) {
					str = "请输入正确的手机号！";
					showdia(str);
				  }else{
					finish();
				}

			}
		});

		regback.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}


		public  void showdia(String str){
			new AlertDialog.Builder(RegActivity.this)
					.setTitle("提示")
					.setMessage(str)
					.setNegativeButton("确定", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					})
					.show();
		}

}
