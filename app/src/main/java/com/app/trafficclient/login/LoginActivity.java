package com.app.trafficclient.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.trafficclient.MainActivity;
import com.app.trafficclient.R;
import com.app.trafficclient.util.HttpRequest;
import com.app.trafficclient.util.LoadingDialog;
import com.app.trafficclient.util.Shared;
import com.app.trafficclient.util.UrlBean;
import com.app.trafficclient.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class LoginActivity extends Activity {

	private UrlBean urlBean;
	private String urlHttp;
	private String urlPort = "8088";
	private int numPort;
	
	EditText accountET, pwdET;
	Button loginBtn, regBtn,settingBtn;
	TextView tv_login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initView();
		initLiserter();
	}

	private void initView() {
		// TODO Auto-generated method stub
		accountET = (EditText) findViewById(R.id.accountET);
		pwdET = (EditText) findViewById(R.id.pwdET);
		loginBtn = (Button) findViewById(R.id.loginBtn);
		regBtn = (Button) findViewById(R.id.regBtn);
		settingBtn = (Button) findViewById(R.id.setting);
		tv_login = findViewById(R.id.tv_login);
		tv_login.requestFocus();//获得焦点

        urlBean = Util.loadSetting( LoginActivity.this );

	}
	
	private void initLiserter() {
		// TODO Auto-generated method stub
		/**注册*/
		regBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this, RegActivity.class);
				startActivity(intent);
			}
		});
/**登陆*/
		loginBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				final String account = accountET.getText().toString();//账号
				String pwd = pwdET.getText().toString();//密码
				if (account.equals("user1") && pwd.equals("123456")) {
					startActivity(new Intent(LoginActivity.this, MainActivity.class));
				}else if (account.equals("user2") && pwd.equals("123456")) {
					startActivity(new Intent(LoginActivity.this, PuTongYongHu.class));
				}else {
					Toast.makeText(getApplicationContext(),"请输入账号密码",Toast.LENGTH_LONG).show();
				}
			}

			});


/**网络设置*/
				settingBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						final Dialog urlSettingDialog = new Dialog(LoginActivity.this);
						urlSettingDialog.show();//在添加内容之前执行
						urlSettingDialog.getWindow().setContentView(R.layout.login_setting);//自定义
						final EditText edit_urlHttp = (EditText) urlSettingDialog.getWindow().findViewById(R.id.edit_setting_url);
						final EditText edit_urlPort = (EditText) urlSettingDialog.getWindow().findViewById(R.id.edit_setting_port);

						//给这两个编辑框，赋值，从setting中读出来的url和端口
						edit_urlHttp.setText(urlBean.getUrl());
						edit_urlPort.setText(urlBean.getPort());

						Button save = (Button) urlSettingDialog.getWindow().findViewById(R.id.save);
						Button cancel = (Button) urlSettingDialog.getWindow().findViewById(R.id.cancel);
/**对话框—保存*/
						save.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {

								// TODO Auto-generated method stub
								urlHttp = edit_urlHttp.getText().toString(); //这是一个字符串，上面定义
								urlPort = edit_urlPort.getText().toString();
								numPort = Integer.parseInt(urlPort);

								if (urlHttp.equals("") || urlHttp.equals("")) {
									//	Toast.makeText(LoginActivity.this,R.string.error_ip, Toast.LENGTH_LONG).show();
									LoadingDialog.showToast(LoginActivity.this, R.string.error_ip);
								} else if (numPort < 0 || numPort >= 65535) {
									LoadingDialog.showToast(LoginActivity.this, R.string.Login_portError);
								} else {
//							Util类的save方法,  参数是两个编辑框内容  和context ，用来保存进setting文件
									Util.saveSetting(urlHttp, urlPort, LoginActivity.this);
									/* util类的loadSetting方法，把保存好的ip和端口从setting中读出来
									 *  用UrlBean类set方法赋值到两个编辑框中
									 */
									urlBean = Util.loadSetting(LoginActivity.this);
									LoadingDialog.showToast(LoginActivity.this, R.string.save_ok);
									urlSettingDialog.dismiss();
								}
							}
						});
						/**取消*/
						cancel.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								urlSettingDialog.dismiss();

							}
						});
						//urlSettingDialog.show(); 感觉没用

					}//网络设置—onclick
				});//网络设置 settingBtn

			}

		}

