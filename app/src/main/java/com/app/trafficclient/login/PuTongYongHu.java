package com.app.trafficclient.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.app.trafficclient.ChuXingGuanLi.ChuXingGuanLI;
import com.app.trafficclient.DiTieChaXun.DiTieChaXun;
import com.app.trafficclient.LuKuangChaXun.LuKuangChaXun;
import com.app.trafficclient.LvXingXinxi.LvXingXinXi;
import com.app.trafficclient.MainActivity;
import com.app.trafficclient.R;
import com.app.trafficclient.ShengHuoZhiShu.LifeIndex;
import com.app.trafficclient.WoDeZuoJia.WoDeZuoJia;
import com.app.trafficclient.ZhangHuGuanli.ZhangHuGuanLi;
import com.app.trafficclient.erWeiMa.ErWeiMa;
import com.app.trafficclient.geRenZhongXin.PersonalCenter;
import com.app.trafficclient.hongLvDengGuanLi;
import com.app.trafficclient.huanJingZhiBiao;
import com.app.trafficclient.shuJuFenXi.ShuJuFenXi;
import com.app.trafficclient.tianQi.WeatherMsg;
import com.app.trafficclient.weiZhangChaKan;
import com.app.trafficclient.woDeZhangHu.MyAccount;
import com.app.trafficclient.yuZhiSheZhi;
import com.app.trafficclient.zhangDanGuanli;

import java.util.ArrayList;
import java.util.HashMap;

public class PuTongYongHu extends AppCompatActivity {

    private SlidingPaneLayout slidepanel;//本质是一个水平的多层布局控件
    private ViewPager mViewPager; //注意这个组件是用来显示左右滑动的界面的，如果不加载xml布局文件，他是不会显示内容的
    BottomNavigationView navigation;//底部导航栏

    private ListView listView;
    //  SimpleAdapter simpleAdapter;
    ArrayList<HashMap<String, Object>> actionItems;
    SimpleAdapter actionAdapter;

    TextView tV_title;
    ImageView iVSliding;
    ImageView ivHome;
    static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initDate();
        initListen();

    }//onCreate

    private void initListen() {

        iVSliding.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (slidepanel.isOpen()) {//判断左侧面板是否打开。
                    slidepanel.closePane();//关闭左侧面板
                } else {
                    slidepanel.openPane();//打开左侧面板
                }
            }
        });


        listView.setAdapter(actionAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stub
                switch (arg2) {
                    case 0:startActivity(new Intent(context,MyAccount.class         ));break;
                    case 1:startActivity(new Intent(context,hongLvDengGuanLi.class           ));break;
                    case 2:startActivity(new Intent(context,zhangDanGuanli.class        ));break;
                    case 3:startActivity(new Intent(context,weiZhangChaKan.class           ));break;
                    case 4:startActivity(new Intent(context,huanJingZhiBiao.class));break;
                    case 5:startActivity(new Intent(context,yuZhiSheZhi.class       ));break;
                    case 6:startActivity(new Intent(context,ZhangHuGuanLi.class               ));break;
                    case 7:startActivity(new Intent(context,PersonalCenter.class    ));break;
                    case 8:startActivity(new Intent(context,WeatherMsg.class        ));break;
                    case 10: exitAppDialog();break;

                    default: break;
                }
            }
        });
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            //参数position，代表哪个页面被选中
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        navigation.getMenu().findItem(R.id.navigation_1).setChecked(true);//表示已经选中
                        break;
                    case 1:
                        navigation.getMenu().findItem(R.id.navigation_3).setChecked(true);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_1:
                        mViewPager.setCurrentItem(0);//0是当前界面
                        return true;
                    case R.id.navigation_3:
                        mViewPager.setCurrentItem(1);
                        return true;
                }

                return false;
            }
        });
    }

    private void initDate() {
        context =PuTongYongHu.this;
        final String[] actionTexts = new String[]{
                getString(R.string.res_left_modulex),//0
                getString(R.string.res_left_redGreen),//1
                getString(R.string.res_left_module3),//2
                getString(R.string.res_left_module4),//3
                "环境指标",//4
                "阈值设置",//5
                "账户管理",//6
                "个人中心", //7
                "天气信息",//8
                getString(R.string.res_left_module5),//9



        };

        int[] actionImages = new int[]{
                R.drawable.btn_l_book, //我的账户 0
                R.drawable.btn_l_book, //红绿灯1
                R.drawable.btn_l_book,//账单2
                R.drawable.btn_l_book,//违章3
                R.drawable.btn_l_book,//环境4
                R.drawable.btn_l_slideshow,//阈值5
                R.drawable.btn_l_share, //账户6
                R.drawable.btn_l_book,//个人中心7
                R.drawable.btn_l_book,//天气 8
                R.drawable.btn_l_book,//关于9

        };

        actionItems = new ArrayList<>();
        actionAdapter = new SimpleAdapter(
                getApplicationContext(),
                actionItems,//数据源
                R.layout.main_left_list_fragment_item,//一个图片+文本框的布局
                new String[]{"action_icon", "action_name"},//将被添加到Map映射上的key。
                new int[]{R.id.recharge_method_icon,R.id.recharge_method_name});  //这两个是图像和文本框的id  和上面对应
        //0  ___  <5
        for (int i = 0; i < actionImages.length; i++) {
            HashMap<String, Object> item1 = new HashMap<>();
            item1.put("action_icon", actionImages[i]);
            item1.put("action_name", actionTexts[i]);
            actionItems.add(item1);
        }
    }

    public static void wangluoIsNull() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("提示")
                .setMessage("网络请求失败！")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    private void initView() {
        slidepanel = (SlidingPaneLayout) findViewById(R.id.slidingPL);
        listView = (ListView) findViewById(R.id.listView1);
        ivHome = (ImageView) findViewById(R.id.imageView_home); //左侧划出的房子
        iVSliding = (ImageView) findViewById(R.id.imageView_back);//左上三条扛
        iVSliding.setImageResource(R.drawable.change);

        tV_title = (TextView) findViewById(R.id.tv_title);
        tV_title.setText("智能交通");
        navigation = (BottomNavigationView) findViewById(R.id.navigation);

        /*初始化显示内容*/
        mViewPager = (ViewPager) findViewById(R.id.pager);
        final ArrayList<Fragment> fgLists = new ArrayList<>();
        // fgLists.add(new Main_frag1());
        //  fgLists.add(new Fragment_2());

        FragmentPagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fgLists.get(position);
            }

            @Override
            public int getCount() {
                return fgLists.size();
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(2); //预加载剩下两页

        /*给底部导航栏菜单项添加点击事件*/

    }

//    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
//            = new BottomNavigationView.OnNavigationItemSelectedListener() {
//        @Override
//        public boolean onNavigationItemSelected( MenuItem item) {
//
//        }
//
//    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        // 按下键盘上返回按钮
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitAppDialog();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

    public void exitAppDialog() {
        new AlertDialog.Builder(this)
                // .setIcon(android.R.drawable.ic_menu_info_detailsp)
                .setTitle("提示")
                .setMessage("你确定要退出吗")
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                }).show();

    }
}
