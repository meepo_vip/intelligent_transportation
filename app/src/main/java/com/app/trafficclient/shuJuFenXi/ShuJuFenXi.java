package com.app.trafficclient.shuJuFenXi;

import android.app.Fragment;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.trafficclient.R;
import com.app.trafficclient.shuJuFenXi.ZhuXing.OnFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class ShuJuFenXi extends AppCompatActivity implements BingXing.OnFragmentInteractionListener,OnFragmentInteractionListener{
    private ViewPager shuJuFenXiPager;
    private RadioGroup shuJuFenXiRgp;
    List<android.support.v4.app.Fragment> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shu_ju_fen_xi);

        shuJuFenXiPager = findViewById(R.id.shuJuFenXi_pager);
        shuJuFenXiRgp = findViewById(R.id.shuJuFenXi_rgp);

        list =new ArrayList<>();
        list.add(new BingXing());
        list.add(new ZhuXing());

        FragAdapter adapter = new FragAdapter(getSupportFragmentManager(),list);
        shuJuFenXiPager.setAdapter(adapter);
        ((RadioButton)shuJuFenXiRgp.getChildAt(0)).setChecked(true);
        initListener();
    }

    private void initListener() {
        shuJuFenXiPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
               if (position == 0){
                   ((RadioButton)shuJuFenXiRgp.getChildAt(0)).setChecked(true);
               }else {
                   ((RadioButton)shuJuFenXiRgp.getChildAt(1)).setChecked(true);
               }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

        });
    }//initListener

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

}//class
