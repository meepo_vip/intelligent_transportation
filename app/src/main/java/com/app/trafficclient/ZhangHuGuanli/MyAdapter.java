package com.app.trafficclient.ZhangHuGuanli;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trafficclient.R;

import java.util.List;
import java.util.Map;

public class MyAdapter extends BaseAdapter {
//这是listview的适配器
    private static Dialog myDialog;
    private List<Map<String, Object>> dataList;
    private static Context context;
    private int layout;

    Util util;
     static  String  carNums[]={""};

   static String[] many = {"","","",""};//单选摁扭操作，删减车号后

    public MyAdapter(List<Map<String, Object>> dataList, Context context, int layout) {
        this.dataList = dataList;
        this.context = context;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // 声明内部类
        /**
         * 根据listView工作原理，列表项的个数只创建屏幕第一次显示的个数。
         * 之后就不会再创建列表项xml文件的对象，以及xml内部的组件，优化内存，性能效率
         */
        if (convertView == null) {
            util = new Util();
            // 给xml布局文件创建java对象
            convertView = LayoutInflater.from(context).inflate(layout,null);

            // 指向布局文件内部组件
            util.id = (TextView) convertView.findViewById(R.id.etc_item_id);
            util.imageView = (ImageView) convertView.findViewById(R.id.etc_item_image);
            util.carNumber = (TextView) convertView.findViewById(R.id.etc_item_carnumber);
            util.msg = (TextView) convertView.findViewById(R.id.etc_item_msg);
            util.balance= (TextView)convertView.findViewById(R.id.etc_item_balance);
            util.checkBox = convertView.findViewById(R.id.etc_item_check);
            util.button = (Button)convertView.findViewById(R.id.etc_item_btn);

            // 增加额外变量
            convertView.setTag(util);
        } else {
            util = (Util) convertView.getTag();
        }
        // 获取数据显示在各组件
        final Map<String, Object> map = dataList.get(position);

        util.id.setText((String)map.get("id"));
        util.imageView.setImageResource((Integer)map.get("img"));
        util.carNumber.setText((String)map.get("carnumber"));
        util.msg.setText((String)map.get("people"));

        util.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                   many[position] = (String)map.get("carnumber");
                    Toast.makeText(context,many[0]+many[1]+many[2]+many[3],Toast.LENGTH_SHORT).show();
                    ZhangHuGuanLi.payMany(many);
                }else {
                    many[position] = "";
                    Toast.makeText(context,many[0]+many[1]+many[2]+many[3],Toast.LENGTH_SHORT).show();
                    ZhangHuGuanLi.payMany(many);
                }
            }
        }
        );

        util.button.setOnClickListener(new View.OnClickListener() { //充值
            @Override
            public void onClick(View v) {
                carNums[0] = (String)map.get("carnumber");
                showAlert(carNums);
//                showAlert(carNum[position]);
            }
        });
        return convertView;
    }

    class Util{
    TextView id,msg,balance,carNumber;
    ImageView imageView;
    CheckBox checkBox;
    Button button;
    }


    public static void showAlert(String carNum[]){
        myDialog =new MyDialog(context,R.layout.viewalert,carNum);
        myDialog.setCanceledOnTouchOutside(true);
        myDialog.show();
    }


public static void cancelDiolog(){
    myDialog.dismiss();
}

}
