package com.app.trafficclient.ZhangHuGuanli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.trafficclient.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZhangHuGuanLi extends AppCompatActivity {
    private ListView listView;
    private List<Map<String,Object>> dataList;
     static String[] manys={"","","",""};
    TextView payMany,payHistory;  //批量充值和历史记录
    private ImageView etcBack;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etc);

        listView=(ListView)findViewById(R.id.etc_listview);
        payMany = findViewById(R.id.etc_paymany);
        etcBack = findViewById(R.id.etc_back);
        tvTitle = findViewById(R.id.tv_title);
        initData();//初始化数据
        initListener();

        MyAdapter adapter = new MyAdapter( dataList, this,R.layout.etc_list_item);//适配器的构造方法
        listView.setAdapter(adapter);
    }

    private void initListener() {
        etcBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        payMany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context,manys[0]+manys[1]+manys[2]+manys[3],Toast.LENGTH_SHORT).show();

              MyAdapter.showAlert(manys);
            }
        });
    }

    private void initData() {
        /**
         * 初始化适配器需要的数据格式
         */
            //车id
            String carIds[]={"1","2","3","4"};
            //车标资源
            int imgs[] = { R.drawable.baoma, R.drawable.zhonghua, R.drawable.benchi,R.drawable.mazhida};
            //车牌和车主
            String peoples[] = {"张三","李四","王五","赵六"};
            String carNumbers[]={"辽A1001","辽A1002","辽A1003","辽A1004"};

            dataList = new ArrayList<Map<String, Object>>();

            for (int i = 0; i < imgs.length; i++) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("id",carIds[i]);
                map.put("img", imgs[i]);
                map.put("carnumber",carNumbers[i]);
                map.put("people", peoples[i]);
//                map.put("balances", balances[i]);

                dataList.add(map);
            }

        }//initData
    public  static  void payMany(String many[]){
       manys =many;
    }



 }
