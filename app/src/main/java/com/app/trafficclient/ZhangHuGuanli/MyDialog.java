package com.app.trafficclient.ZhangHuGuanli;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.trafficclient.R;


public class MyDialog extends Dialog {
Context context;
TextView textView;
Button pay,cancel;
EditText editText;
String num="";

                          //上下文   布局，   车号
    public MyDialog(final Context context, final int layout,String carNum[]) {
        super(context);
        this.context= context;
        setContentView(layout);

        textView = findViewById(R.id.alert_carnumber);//车号
        editText = findViewById(R.id.alert_input); //输入充值金额
        cancel = findViewById(R.id.alert_cancel);
        pay = findViewById(R.id.alert_pay);

        for (int i = 0;i<carNum.length;i++){
            if (i==2){
                num+="\n";
            }
            num += carNum[i];

        }
        textView.setText(num);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.length()>0 && editText.length()<4){
                    Toast.makeText(context,"222",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context,"请输入1-999之间的金额！",Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              MyAdapter.cancelDiolog();
            }
        });

    }


}
