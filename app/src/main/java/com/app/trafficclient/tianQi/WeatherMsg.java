package com.app.trafficclient.tianQi;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.app.trafficclient.R;
import com.app.trafficclient.util.MyLoadDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.google.gson.internal.bind.util.ISO8601Utils.format;

public class WeatherMsg extends AppCompatActivity {
    //标题栏
    private ImageView weatherBackIv;
    private TextView weatherTitleTv;
    private ImageView weatherRefreshIv;
    //标题栏下，今日天气图，时间，地点，温度
    private ImageView todayWeatherTitleIv;
    private TextView todayTimeTitleTv;
    private TextView todayAddressTitleTv;
    private TextView todayTemperatureTitleTv;

    private TextView forecastTv;//预报那两个字，没用

    private RelativeLayout weatherRelaRongQi1;
    private TextView weatherRelaRongQi1TvRiZhou;
    private ImageView weatherRelaRongQi1IvTuPian;
    private TextView weatherRelaRongQi1TvTianQi;
    private TextView weatherRelaRongQi1TvWenDu;

    private RelativeLayout weatherRelaRongQi2;
    private TextView weatherRelaRongQi2TvRiZhou;
    private ImageView weatherRelaRongQi2IvTuPian;
    private TextView weatherRelaRongQi2TvTianQi;
    private TextView weatherRelaRongQi2TvWenDu;

    private RelativeLayout weatherRelaRongQi3;
    private TextView weatherRelaRongQi3TvRiZhou;
    private ImageView weatherRelaRongQi3IvTuPian;
    private TextView weatherRelaRongQi3TvTianQi;
    private TextView weatherRelaRongQi3TvWenDu;

    private RelativeLayout weatherRelaRongQi4;
    private TextView weatherRelaRongQi4TvRiZhou;
    private ImageView weatherRelaRongQi4IvTuPian;
    private TextView weatherRelaRongQi4TvTianQi;
    private TextView weatherRelaRongQi4TvWenDu;

    private RelativeLayout weatherRelaRongQi5;
    private TextView weatherRelaRongQi5TvRiZhou;
    private ImageView weatherRelaRongQi5IvTuPian;
    private TextView weatherRelaRongQi5TvTianQi;
    private TextView weatherRelaRongQi5TvWenDu;


    SimpleDateFormat DtoS;// = new SimpleDateFormat("yyyy年MM月dd日 EE");
    SimpleDateFormat StoD ;//= new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat getRi;
    SimpleDateFormat getZhou;
    Date[] date =new Date[6]; //放5天的时间
    TextView[] allRiqi  = new TextView[5];
    TextView[] allWendu = new TextView[5];
    String [] tian = new String[]{"(今天)","(明天)","(后天)"};
    String url = "http://www.wpwh.info/environment/weather";
    RequestQueue requestQueue;
    MyLoadDialog myLoadDialog;
    Context context =WeatherMsg.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        initView();
        getWeather(1);
    }
    private void dismissdialog() {
        myLoadDialog.dismiss();
    }

    private void showAlertDialog() {
        myLoadDialog = new MyLoadDialog(context);
        myLoadDialog.setCancelable(true);
        myLoadDialog.show();
    }

    private void getWeather(final int flag) {
        showAlertDialog();
        Map<String, String> map = new HashMap<>();
        map.put("username", "user1");
        JSONObject jsonObject = new JSONObject(map);

        final JsonRequest<JSONObject> jsonObjectJsonRequest =new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                       String s = jsonObject.toString();
//                        Log.d("WEA!!!",s);
                        Gson gson = new Gson();
                       WeatherBean  weatherBean = gson.fromJson(s,WeatherBean.class);
                        todayTemperatureTitleTv.setText(weatherBean.getCurTemperature());
                       if (flag == 1) {
                           for (int i = 0; i < 5; i++) {
                               try {
                                   //往数组里放格式化的日期数据
                                   date[i] = StoD.parse(weatherBean.getWeatherPredictions().get(i).getData());
                                   allWendu[i].setText(weatherBean.getWeatherPredictions().get(i).getWeather() + "℃");
                                   if (i < 3) {
                                       allRiqi[i].setText(getRi.format(date[i]) + tian[i]);
                                   } else {
                                       allRiqi[i].setText(getRi.format(date[i]) + getZhou.format(date[i]));
                                   }
                               } catch (ParseException e) { e.printStackTrace(); }
                           }//for
                       } //if
                         todayTimeTitleTv.setText(DtoS.format(date[0]));
                         dismissdialog();
                    }//onResponse
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
               Log.d("WEA!!!",volleyError.toString());
               dismissdialog();
            }
        });
        requestQueue.add(jsonObjectJsonRequest);
    }//getWeather

    private void initView() {
        StoD    = new SimpleDateFormat("yyyy-MM-dd");
        DtoS    = new SimpleDateFormat("yyyy年MM月dd日 EE");
        getRi   = new SimpleDateFormat("dd日");
        getZhou = new SimpleDateFormat("(EE)");

        weatherBackIv = findViewById(R.id.weather_back_iv);
        weatherTitleTv = findViewById(R.id.weather_title_tv);
        weatherRefreshIv = findViewById(R.id.weather_refresh_iv);


        todayWeatherTitleIv = findViewById(R.id.today_weather_title_iv); //今日天气图标
        todayTimeTitleTv = findViewById(R.id.today_time_title_tv); //今日时间
        todayAddressTitleTv = findViewById(R.id.today_address_title_tv); //地点
        todayTemperatureTitleTv = findViewById(R.id.today_temperature_title_tv);//今日温度

        weatherRefreshIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeather(0);
//                todayTemperatureTitleTv.setText(weatherBean.getCurTemperature());
            }
        });
//        forecastTv = findViewById(R.id.forecast_tv);  没用

        weatherRelaRongQi1 = findViewById(R.id.weather_rela_rongQi1);
        weatherRelaRongQi1TvRiZhou = findViewById(R.id.weather_rela_rongQi1_tv_riZhou);
        weatherRelaRongQi1IvTuPian = findViewById(R.id.weather_rela_rongQi1_iv_tuPian);
        weatherRelaRongQi1TvTianQi = findViewById(R.id.weather_rela_rongQi1_tv_tianQi);
        weatherRelaRongQi1TvWenDu = findViewById(R.id.weather_rela_rongQi1_tv_wenDu);

        weatherRelaRongQi2 = findViewById(R.id.weather_rela_rongQi2);
        weatherRelaRongQi2TvRiZhou = findViewById(R.id.weather_rela_rongQi2_tv_riZhou);
        weatherRelaRongQi2IvTuPian = findViewById(R.id.weather_rela_rongQi2_iv_tuPian);
        weatherRelaRongQi2TvTianQi = findViewById(R.id.weather_rela_rongQi2_tv_tianQi);
        weatherRelaRongQi2TvWenDu = findViewById(R.id.weather_rela_rongQi2_tv_wenDu);

        weatherRelaRongQi3 = findViewById(R.id.weather_rela_rongQi3);
        weatherRelaRongQi3TvRiZhou = findViewById(R.id.weather_rela_rongQi3_tv_riZhou);
        weatherRelaRongQi3IvTuPian = findViewById(R.id.weather_rela_rongQi3_iv_tuPian);
        weatherRelaRongQi3TvTianQi = findViewById(R.id.weather_rela_rongQi3_tv_tianQi);
        weatherRelaRongQi3TvWenDu = findViewById(R.id.weather_rela_rongQi3_tv_wenDu);

        weatherRelaRongQi4 = findViewById(R.id.weather_rela_rongQi4);
        weatherRelaRongQi4TvRiZhou = findViewById(R.id.weather_rela_rongQi4_tv_riZhou);
        weatherRelaRongQi4IvTuPian = findViewById(R.id.weather_rela_rongQi4_iv_tuPian);
        weatherRelaRongQi4TvTianQi = findViewById(R.id.weather_rela_rongQi4_tv_tianQi);
        weatherRelaRongQi4TvWenDu = findViewById(R.id.weather_rela_rongQi4_tv_wenDu);

        weatherRelaRongQi5 = findViewById(R.id.weather_rela_rongQi5);
        weatherRelaRongQi5TvRiZhou = findViewById(R.id.weather_rela_rongQi5_tv_riZhou);
        weatherRelaRongQi5IvTuPian = findViewById(R.id.weather_rela_rongQi5_iv_tuPian);
        weatherRelaRongQi5TvTianQi = findViewById(R.id.weather_rela_rongQi5_tv_tianQi);
        weatherRelaRongQi5TvWenDu = findViewById(R.id.weather_rela_rongQi5_tv_wenDu);


        allRiqi[0] = weatherRelaRongQi1TvRiZhou;
        allRiqi[1] = weatherRelaRongQi2TvRiZhou;
        allRiqi[2] = weatherRelaRongQi3TvRiZhou;
        allRiqi[3] = weatherRelaRongQi4TvRiZhou;
        allRiqi[4] = weatherRelaRongQi5TvRiZhou;

        allWendu[0] = weatherRelaRongQi1TvWenDu;
        allWendu[1] = weatherRelaRongQi2TvWenDu;
        allWendu[2] = weatherRelaRongQi3TvWenDu;
        allWendu[3] = weatherRelaRongQi4TvWenDu;
        allWendu[4] = weatherRelaRongQi5TvWenDu;
    }


}
