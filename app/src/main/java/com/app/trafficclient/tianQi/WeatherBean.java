package com.app.trafficclient.tianQi;

import java.util.List;

public class WeatherBean {

    /**
     * weatherPredictions : [
     * {
     * "data":"2019-03-21",
     * "weather":"5~12"},
     *
     * {"data":"2019-03-22",
     * "weather":"7~32"},
     *
     * {"data":"2019-03-23",
     * "weather":"25~39"},
     *
     * {"data":"2019-03-24",
     * "weather":"28~32"},
     *
     * {"data":"2019-03-25",
     * "weather":"24~26"},
     *
     * {"data":"2019-03-26",
     * "weather":"2~4"}
     * ]
     * statusInfo : 请求成功
     * curTemperature : 9
     * status : 1
     */
    private List<WeatherPredictionsEntity> weatherPredictions;
    private String statusInfo;
    private String curTemperature;
    private int status;

    public void setWeatherPredictions(List<WeatherPredictionsEntity> weatherPredictions) {
        this.weatherPredictions = weatherPredictions;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public void setCurTemperature(String curTemperature) {
        this.curTemperature = curTemperature;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<WeatherPredictionsEntity> getWeatherPredictions() {
        return weatherPredictions;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public String getCurTemperature() {
        return curTemperature;
    }

    public int getStatus() {
        return status;
    }

    public class WeatherPredictionsEntity {
        /**
         * data : 2019-03-21
         * weather : 5~12
         */
        private String data;
        private String weather;

        public void setData(String data) {
            this.data = data;
        }

        public void setWeather(String weather) {
            this.weather = weather;
        }

        public String getData() {
            return data;
        }

        public String getWeather() {
            return weather;
        }
    }
}
