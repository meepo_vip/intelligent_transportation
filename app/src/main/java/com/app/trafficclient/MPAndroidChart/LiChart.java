package com.app.trafficclient.MPAndroidChart;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

public class LiChart {
    //折线统计图
    public static void  setLineChart(List<List> val, List<String> name, List<String> lable,List<Integer> color, LineChart lineChart,String chartname){
        lineChart.setNoDataTextDescription("无数据");
        lineChart.setDescription(chartname);
        lineChart.setDrawBorders(true);
        lineChart.setDrawMarkerViews(true);
        lineChart.setDrawGridBackground(false);
        lineChart.setDoubleTapToZoomEnabled(false);
        XAxis x=lineChart.getXAxis();
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setSpaceBetweenLabels(0); // 设置数据之间的间距'
        lineChart.getAxisRight().setEnabled(false);
        List<LineDataSet> lineDataSetList=new ArrayList<>();
        for (int a=0;a<val.size();a++) {
            List<Entry> entryList = new ArrayList<>();
            List<Integer> list=val.get(a);
            for (int i = 0; i < val.get(a).size(); i++) {
                entryList.add(new Entry(list.get(i), i));
            }
            LineDataSet lineDataSet=new LineDataSet(entryList,lable.get(a));
            lineDataSet.setCircleColor(color.get(a));
            lineDataSet.setColor(color.get(a));
            lineDataSetList.add(lineDataSet);
        }

        LineData lineData=new LineData(name,lineDataSetList);
        lineChart.setData(lineData);

    }
}
