package com.app.trafficclient.MPAndroidChart;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class PiChart {
    public static void setPieChart(PieChart pieChart, List<Float> val,List<String> name,String pieNmae){
        pieChart.setNoDataTextDescription("无数据哦");

        pieChart.setDescription(pieNmae);
        pieChart.setRotationEnabled(false);
        List<Entry> entryList=new ArrayList<>();
        for (int i=0;i<val.size();i++){
            entryList.add(new Entry(val.get(i),i));
        }
            List<Integer> colors=new ArrayList<>();
        for (int i:ColorTemplate.COLORFUL_COLORS){
            colors.add(i);
        }
        for (int i:ColorTemplate.LIBERTY_COLORS){
            colors.add(i);
        }
        for (int i:ColorTemplate.VORDIPLOM_COLORS){
            colors.add(i);
        }
        for (int i:ColorTemplate.PASTEL_COLORS){
            colors.add(i);
        }

        PieDataSet pieDataSet=new PieDataSet(entryList,"");
        pieDataSet.setColors(colors);
        pieDataSet.setValueTextSize(15);
        PieData pieData=new PieData(name,pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieChart.getLegend().setWordWrapEnabled(true);
        pieChart.setData(pieData);
        pieChart.invalidate();








    }


}
